/**
 * Define a grammar called Hello
 */
 
grammar FlowUmi;

/*jorge franco ibanez
kevin julio marquez
anglel jimenes escobar*/

//declara si es una variable una operacion (asignacion, igualacion) , sentencias
program:	inicio (declarar)*  fin EOF;  

//sentencias
inicio	:	 'Inicio'|'inicio';
fin		:	 'Fin'|'fin';

declarar:	(variable | operacion | sentencia | expresion)*;

//declarar si, si anidados o si - sino
sentencia	:	si
			|	mientras
			|	entre
			| 	hacerMientras
			|	para
			|	tipo NOMBRE bloque PUNTOYCOMA
			|	tipo NOMBRE PI (variablesentencias (',')*) *  PD bloquesentencias
			|	'void' NOMBRE PI (variablesentencias (',')*) * PD bloqueVoid
			;
	
para		:	 'para' PI  ('int' NOMBRE '=' (NOMBRE|INTEGER ) | NOMBRE | NOMBRE '=' ( NOMBRE   | INTEGER))PUNTOYCOMA expresioncondicional PUNTOYCOMA 
			NOMBRE forincrement
			PD bloquep ;

hacerMientras	:	'hacer' bloquep 'mientras' PI expresioncondicional PD PUNTOYCOMA;

mientras	:	'mientras' PI expresioncondicional* PD bloquep;	


si		:	'si' PI expresion PD bloquep sino*;
sino	:	'sino' bloquep;

entre	:	'entre' PI expresion PD bloqueswitchcase ;

bloqueswitchcase	
		:	LLI ('caso' NOMBRE DOSPUNTOS  bloqueswitch )+ ('default' DOSPUNTOS bloqueswitch)*LLD;
expresion
		:	expresioncondicional
		|	expresionAditiva 
		|	expresionInvocacion;
expresionInvocacion
		:	NOMBRE PI (tipo (NOMBRE|valor))* PD PUNTOYCOMA;

expresioncondicional	
		:	expresionLogica;
expresionLogica
		:	expresionBooleana (OR expresionBooleana)*;
expresionBooleana
		:	expresionIgualdad ((EQUALS|NOTEQUALS)expresionIgualdad)*;
expresionIgualdad
		:	expresionRelacional (AND expresionRelacional)*;

expresionRelacional 
		:	expresionAditiva ( (LT|LTEQ|GT|GTEQ) expresionAditiva)*;
	
expresionAditiva
		:	expressionMultiplicativa( (PLUS|MINUS) expressionMultiplicativa)*;
expressionMultiplicativa
		:	expresionPotencia ( (MULT|DIV|MOD) expresionPotencia)*;
expresionPotencia
	:	expresionCerrada  (POW expresionCerrada)*;


expresionCerrada
	:	primaria
	|	NOT primaria
	|	MINUS expresionCerrada
	;

primaria
	:	valor
	;

valor	
	:	INTEGER
	|	FLOAT
	|	NOMBRE
	;
operacion
	:	NOMBRE IGUAL expresion PUNTOYCOMA
	|	NOMBRE CCI (numero|NOMBRE) CCD IGUAL (numero|NOMBRE) PUNTOYCOMA
	|	postfix NOMBRE PUNTOYCOMA
	|	NOMBRE postfix PUNTOYCOMA
	|	NOMBRE assignment (NOMBRE|numero) PUNTOYCOMA
	;




variable
	:  constante tipo NOMBRE PUNTOYCOMA
	| constante simple NOMBRE IGUAL numero PUNTOYCOMA
	| constante tReference NOMBRE IGUAL (NOMBRE| '\u0022' NOMBRE '\u0022')  PUNTOYCOMA
	| tipo NOMBRE bloque PUNTOYCOMA
	| tipo NOMBRE PUNTOYCOMA 
	| tipo NOMBRE (',' NOMBRE)* PUNTOYCOMA 
	| tipo NOMBRE (',' NOMBRE)* IGUAL NOMBRE (',' NOMBRE)* PUNTOYCOMA 
	| tipo NOMBRE (',' NOMBRE)* IGUAL numero (',' numero)* PUNTOYCOMA 
	| tipo NOMBRE IGUAL NOMBRE PUNTOYCOMA 
	| tipo NOMBRE IGUAL numero PUNTOYCOMA
	| 'char' NOMBRE IGUAL  '\u0027' CHAR '\u0027' PUNTOYCOMA
	| 'char' CCI CCD NOMBRE IGUAL 'new' 'char' CCI (numero) CCD PUNTOYCOMA
	| tipo CCI CCD NOMBRE IGUAL 'new' tipo CCI (numero|NOMBRE) CCD PUNTOYCOMA
	| tipo CCI CCD NOMBRE IGUAL bloque PUNTOYCOMA
	;
	
variablesentencias
	:  constante tipo NOMBRE 
	| constante simple NOMBRE IGUAL numero 
	| constante tReference NOMBRE IGUAL (NOMBRE| '\u0022' NOMBRE '\u0022')  
	| tipo NOMBRE  
	| tipo NOMBRE IGUAL NOMBRE  
	| tipo NOMBRE IGUAL numero 
	| 'char' NOMBRE IGUAL  '\u0027' CHAR '\u0027' 
	| 'char' CCI CCD NOMBRE IGUAL 'new' 'char' CCI (numero) CCD 
	;



tipo		:	tvalor;
/*valor*/
tvalor		:	simple;
simple		:	integral|flotante|decimal|booleano;
integral		:	'int'|'uint'|'short'|'ushort'|'long'|'ulong'|'byte'|'sbyte';
flotante		:	'float'|'double'|'long double';
decimal		:	'decimal';
booleano		:	'bool';
enumerador		:	'enum';
tReference		:	'class' |'interface' |'delegate' |'dynamic' |'Object' |'string' ;     //https://msdn.microsoft.com/es-es/library/490f96s2.aspx
constante		:	'Const';
numero			:	INTEGER|FLOAT;
bloque	 		:	LLI ((NOMBRE (',' NOMBRE)*)| (numero(',' numero)*)) LLD;
bloquep			:	LLI (variable|operacion|sentencia)* LLD;
bloqueswitch		:	(variable|operacion|sentencia)* 'break' PUNTOYCOMA;
bloquesentencias	:	LLI (variable|operacion|sentencia)* 'return' valor PUNTOYCOMA LLD ;
bloqueVoid		:	LLI (variable|operacion|sentencia)* LLD;
postfix			:	LLI LLD |CCI CCD |'->' |PLUS PLUS |MINUS MINUS;
assignment		:	PLUS IGUAL| MINUS IGUAL| MULT IGUAL | DIV IGUAL | MOD IGUAL | POW IGUAL;
forincrement	:	PLUS PLUS |MINUS MINUS;


NOMBRE	:	('a'..'z' |  'A'..'Z'  | '_')('a'..'z' |  'A'..'Z'  |  '0'..'9')*;
INTEGER	:	('0'..'9')+;
CHAR	:	('a'..'z' |  'A'..'Z'  |  '0'..'9');
FLOAT	:	('0'..'9')* '.' ('0'..'9')+;
PUNTOYCOMA :	';';	
DOSPUNTOS	:	':';
IGUAL 	:	'=';
LLI	:	'{';
LLD	:	'}';
CCI	:	'[';
CCD	:	']';
PI	:	'(';
PD	:	')';

NOT	:	'!' | 'not';
PLUS	:	'+';
MINUS	:	'-';
MULT	:	'*';
DIV	:	'/';
MOD	:	'%';
POW	:	'^';


EQUALS	:	'=' | '==';
NOTEQUALS 	:	'!=' | '<>';
LT	:	'<';
LTEQ	:	'<=';
GTEQ	:	'>=';
GT	:	'>';
OR 	: 	'||' | 'or';
AND 	: 	'&&' | 'and';