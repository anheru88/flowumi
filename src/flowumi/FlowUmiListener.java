package flowumi;

// Generated from FlowUmi.g4 by ANTLR 4.4
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link FlowUmiParser}.
 */
public interface FlowUmiListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionBooleana}.
	 * @param ctx the parse tree
	 */
	void enterExpresionBooleana(@NotNull FlowUmiParser.ExpresionBooleanaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionBooleana}.
	 * @param ctx the parse tree
	 */
	void exitExpresionBooleana(@NotNull FlowUmiParser.ExpresionBooleanaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#variablesentencias}.
	 * @param ctx the parse tree
	 */
	void enterVariablesentencias(@NotNull FlowUmiParser.VariablesentenciasContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#variablesentencias}.
	 * @param ctx the parse tree
	 */
	void exitVariablesentencias(@NotNull FlowUmiParser.VariablesentenciasContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#tipo}.
	 * @param ctx the parse tree
	 */
	void enterTipo(@NotNull FlowUmiParser.TipoContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#tipo}.
	 * @param ctx the parse tree
	 */
	void exitTipo(@NotNull FlowUmiParser.TipoContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#numero}.
	 * @param ctx the parse tree
	 */
	void enterNumero(@NotNull FlowUmiParser.NumeroContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#numero}.
	 * @param ctx the parse tree
	 */
	void exitNumero(@NotNull FlowUmiParser.NumeroContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#bloqueswitchcase}.
	 * @param ctx the parse tree
	 */
	void enterBloqueswitchcase(@NotNull FlowUmiParser.BloqueswitchcaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#bloqueswitchcase}.
	 * @param ctx the parse tree
	 */
	void exitBloqueswitchcase(@NotNull FlowUmiParser.BloqueswitchcaseContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#inicio}.
	 * @param ctx the parse tree
	 */
	void enterInicio(@NotNull FlowUmiParser.InicioContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#inicio}.
	 * @param ctx the parse tree
	 */
	void exitInicio(@NotNull FlowUmiParser.InicioContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#fin}.
	 * @param ctx the parse tree
	 */
	void enterFin(@NotNull FlowUmiParser.FinContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#fin}.
	 * @param ctx the parse tree
	 */
	void exitFin(@NotNull FlowUmiParser.FinContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionCerrada}.
	 * @param ctx the parse tree
	 */
	void enterExpresionCerrada(@NotNull FlowUmiParser.ExpresionCerradaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionCerrada}.
	 * @param ctx the parse tree
	 */
	void exitExpresionCerrada(@NotNull FlowUmiParser.ExpresionCerradaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#simple}.
	 * @param ctx the parse tree
	 */
	void enterSimple(@NotNull FlowUmiParser.SimpleContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#simple}.
	 * @param ctx the parse tree
	 */
	void exitSimple(@NotNull FlowUmiParser.SimpleContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#forincrement}.
	 * @param ctx the parse tree
	 */
	void enterForincrement(@NotNull FlowUmiParser.ForincrementContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#forincrement}.
	 * @param ctx the parse tree
	 */
	void exitForincrement(@NotNull FlowUmiParser.ForincrementContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull FlowUmiParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull FlowUmiParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#primaria}.
	 * @param ctx the parse tree
	 */
	void enterPrimaria(@NotNull FlowUmiParser.PrimariaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#primaria}.
	 * @param ctx the parse tree
	 */
	void exitPrimaria(@NotNull FlowUmiParser.PrimariaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#operacion}.
	 * @param ctx the parse tree
	 */
	void enterOperacion(@NotNull FlowUmiParser.OperacionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#operacion}.
	 * @param ctx the parse tree
	 */
	void exitOperacion(@NotNull FlowUmiParser.OperacionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#bloquep}.
	 * @param ctx the parse tree
	 */
	void enterBloquep(@NotNull FlowUmiParser.BloquepContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#bloquep}.
	 * @param ctx the parse tree
	 */
	void exitBloquep(@NotNull FlowUmiParser.BloquepContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#tvalor}.
	 * @param ctx the parse tree
	 */
	void enterTvalor(@NotNull FlowUmiParser.TvalorContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#tvalor}.
	 * @param ctx the parse tree
	 */
	void exitTvalor(@NotNull FlowUmiParser.TvalorContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionIgualdad}.
	 * @param ctx the parse tree
	 */
	void enterExpresionIgualdad(@NotNull FlowUmiParser.ExpresionIgualdadContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionIgualdad}.
	 * @param ctx the parse tree
	 */
	void exitExpresionIgualdad(@NotNull FlowUmiParser.ExpresionIgualdadContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresion}.
	 * @param ctx the parse tree
	 */
	void enterExpresion(@NotNull FlowUmiParser.ExpresionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresion}.
	 * @param ctx the parse tree
	 */
	void exitExpresion(@NotNull FlowUmiParser.ExpresionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#para}.
	 * @param ctx the parse tree
	 */
	void enterPara(@NotNull FlowUmiParser.ParaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#para}.
	 * @param ctx the parse tree
	 */
	void exitPara(@NotNull FlowUmiParser.ParaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#booleano}.
	 * @param ctx the parse tree
	 */
	void enterBooleano(@NotNull FlowUmiParser.BooleanoContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#booleano}.
	 * @param ctx the parse tree
	 */
	void exitBooleano(@NotNull FlowUmiParser.BooleanoContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#constante}.
	 * @param ctx the parse tree
	 */
	void enterConstante(@NotNull FlowUmiParser.ConstanteContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#constante}.
	 * @param ctx the parse tree
	 */
	void exitConstante(@NotNull FlowUmiParser.ConstanteContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#declarar}.
	 * @param ctx the parse tree
	 */
	void enterDeclarar(@NotNull FlowUmiParser.DeclararContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#declarar}.
	 * @param ctx the parse tree
	 */
	void exitDeclarar(@NotNull FlowUmiParser.DeclararContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#si}.
	 * @param ctx the parse tree
	 */
	void enterSi(@NotNull FlowUmiParser.SiContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#si}.
	 * @param ctx the parse tree
	 */
	void exitSi(@NotNull FlowUmiParser.SiContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#integral}.
	 * @param ctx the parse tree
	 */
	void enterIntegral(@NotNull FlowUmiParser.IntegralContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#integral}.
	 * @param ctx the parse tree
	 */
	void exitIntegral(@NotNull FlowUmiParser.IntegralContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#mientras}.
	 * @param ctx the parse tree
	 */
	void enterMientras(@NotNull FlowUmiParser.MientrasContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#mientras}.
	 * @param ctx the parse tree
	 */
	void exitMientras(@NotNull FlowUmiParser.MientrasContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#flotante}.
	 * @param ctx the parse tree
	 */
	void enterFlotante(@NotNull FlowUmiParser.FlotanteContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#flotante}.
	 * @param ctx the parse tree
	 */
	void exitFlotante(@NotNull FlowUmiParser.FlotanteContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPostfix(@NotNull FlowUmiParser.PostfixContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPostfix(@NotNull FlowUmiParser.PostfixContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#entre}.
	 * @param ctx the parse tree
	 */
	void enterEntre(@NotNull FlowUmiParser.EntreContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#entre}.
	 * @param ctx the parse tree
	 */
	void exitEntre(@NotNull FlowUmiParser.EntreContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#bloque}.
	 * @param ctx the parse tree
	 */
	void enterBloque(@NotNull FlowUmiParser.BloqueContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#bloque}.
	 * @param ctx the parse tree
	 */
	void exitBloque(@NotNull FlowUmiParser.BloqueContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#sino}.
	 * @param ctx the parse tree
	 */
	void enterSino(@NotNull FlowUmiParser.SinoContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#sino}.
	 * @param ctx the parse tree
	 */
	void exitSino(@NotNull FlowUmiParser.SinoContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#tReference}.
	 * @param ctx the parse tree
	 */
	void enterTReference(@NotNull FlowUmiParser.TReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#tReference}.
	 * @param ctx the parse tree
	 */
	void exitTReference(@NotNull FlowUmiParser.TReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionLogica}.
	 * @param ctx the parse tree
	 */
	void enterExpresionLogica(@NotNull FlowUmiParser.ExpresionLogicaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionLogica}.
	 * @param ctx the parse tree
	 */
	void exitExpresionLogica(@NotNull FlowUmiParser.ExpresionLogicaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(@NotNull FlowUmiParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(@NotNull FlowUmiParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#valor}.
	 * @param ctx the parse tree
	 */
	void enterValor(@NotNull FlowUmiParser.ValorContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#valor}.
	 * @param ctx the parse tree
	 */
	void exitValor(@NotNull FlowUmiParser.ValorContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#enumerador}.
	 * @param ctx the parse tree
	 */
	void enterEnumerador(@NotNull FlowUmiParser.EnumeradorContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#enumerador}.
	 * @param ctx the parse tree
	 */
	void exitEnumerador(@NotNull FlowUmiParser.EnumeradorContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#sentencia}.
	 * @param ctx the parse tree
	 */
	void enterSentencia(@NotNull FlowUmiParser.SentenciaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#sentencia}.
	 * @param ctx the parse tree
	 */
	void exitSentencia(@NotNull FlowUmiParser.SentenciaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#bloqueVoid}.
	 * @param ctx the parse tree
	 */
	void enterBloqueVoid(@NotNull FlowUmiParser.BloqueVoidContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#bloqueVoid}.
	 * @param ctx the parse tree
	 */
	void exitBloqueVoid(@NotNull FlowUmiParser.BloqueVoidContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionInvocacion}.
	 * @param ctx the parse tree
	 */
	void enterExpresionInvocacion(@NotNull FlowUmiParser.ExpresionInvocacionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionInvocacion}.
	 * @param ctx the parse tree
	 */
	void exitExpresionInvocacion(@NotNull FlowUmiParser.ExpresionInvocacionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#bloquesentencias}.
	 * @param ctx the parse tree
	 */
	void enterBloquesentencias(@NotNull FlowUmiParser.BloquesentenciasContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#bloquesentencias}.
	 * @param ctx the parse tree
	 */
	void exitBloquesentencias(@NotNull FlowUmiParser.BloquesentenciasContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionRelacional}.
	 * @param ctx the parse tree
	 */
	void enterExpresionRelacional(@NotNull FlowUmiParser.ExpresionRelacionalContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionRelacional}.
	 * @param ctx the parse tree
	 */
	void exitExpresionRelacional(@NotNull FlowUmiParser.ExpresionRelacionalContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#bloqueswitch}.
	 * @param ctx the parse tree
	 */
	void enterBloqueswitch(@NotNull FlowUmiParser.BloqueswitchContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#bloqueswitch}.
	 * @param ctx the parse tree
	 */
	void exitBloqueswitch(@NotNull FlowUmiParser.BloqueswitchContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expressionMultiplicativa}.
	 * @param ctx the parse tree
	 */
	void enterExpressionMultiplicativa(@NotNull FlowUmiParser.ExpressionMultiplicativaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expressionMultiplicativa}.
	 * @param ctx the parse tree
	 */
	void exitExpressionMultiplicativa(@NotNull FlowUmiParser.ExpressionMultiplicativaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(@NotNull FlowUmiParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(@NotNull FlowUmiParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#hacerMientras}.
	 * @param ctx the parse tree
	 */
	void enterHacerMientras(@NotNull FlowUmiParser.HacerMientrasContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#hacerMientras}.
	 * @param ctx the parse tree
	 */
	void exitHacerMientras(@NotNull FlowUmiParser.HacerMientrasContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresioncondicional}.
	 * @param ctx the parse tree
	 */
	void enterExpresioncondicional(@NotNull FlowUmiParser.ExpresioncondicionalContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresioncondicional}.
	 * @param ctx the parse tree
	 */
	void exitExpresioncondicional(@NotNull FlowUmiParser.ExpresioncondicionalContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionPotencia}.
	 * @param ctx the parse tree
	 */
	void enterExpresionPotencia(@NotNull FlowUmiParser.ExpresionPotenciaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionPotencia}.
	 * @param ctx the parse tree
	 */
	void exitExpresionPotencia(@NotNull FlowUmiParser.ExpresionPotenciaContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#decimal}.
	 * @param ctx the parse tree
	 */
	void enterDecimal(@NotNull FlowUmiParser.DecimalContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#decimal}.
	 * @param ctx the parse tree
	 */
	void exitDecimal(@NotNull FlowUmiParser.DecimalContext ctx);
	/**
	 * Enter a parse tree produced by {@link FlowUmiParser#expresionAditiva}.
	 * @param ctx the parse tree
	 */
	void enterExpresionAditiva(@NotNull FlowUmiParser.ExpresionAditivaContext ctx);
	/**
	 * Exit a parse tree produced by {@link FlowUmiParser#expresionAditiva}.
	 * @param ctx the parse tree
	 */
	void exitExpresionAditiva(@NotNull FlowUmiParser.ExpresionAditivaContext ctx);
}