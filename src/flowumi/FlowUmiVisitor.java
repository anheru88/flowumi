package flowumi;

// Generated from FlowUmi.g4 by ANTLR 4.4
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FlowUmiParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface FlowUmiVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionBooleana}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionBooleana(@NotNull FlowUmiParser.ExpresionBooleanaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#variablesentencias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariablesentencias(@NotNull FlowUmiParser.VariablesentenciasContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#tipo}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTipo(@NotNull FlowUmiParser.TipoContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#numero}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumero(@NotNull FlowUmiParser.NumeroContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#bloqueswitchcase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloqueswitchcase(@NotNull FlowUmiParser.BloqueswitchcaseContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#inicio}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInicio(@NotNull FlowUmiParser.InicioContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#fin}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFin(@NotNull FlowUmiParser.FinContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionCerrada}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionCerrada(@NotNull FlowUmiParser.ExpresionCerradaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#simple}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimple(@NotNull FlowUmiParser.SimpleContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#forincrement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForincrement(@NotNull FlowUmiParser.ForincrementContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(@NotNull FlowUmiParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#primaria}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaria(@NotNull FlowUmiParser.PrimariaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#operacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperacion(@NotNull FlowUmiParser.OperacionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#bloquep}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloquep(@NotNull FlowUmiParser.BloquepContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#tvalor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTvalor(@NotNull FlowUmiParser.TvalorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionIgualdad}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionIgualdad(@NotNull FlowUmiParser.ExpresionIgualdadContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresion(@NotNull FlowUmiParser.ExpresionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#para}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPara(@NotNull FlowUmiParser.ParaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#booleano}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleano(@NotNull FlowUmiParser.BooleanoContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#constante}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstante(@NotNull FlowUmiParser.ConstanteContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#declarar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarar(@NotNull FlowUmiParser.DeclararContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#si}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSi(@NotNull FlowUmiParser.SiContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#integral}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegral(@NotNull FlowUmiParser.IntegralContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#mientras}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMientras(@NotNull FlowUmiParser.MientrasContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#flotante}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFlotante(@NotNull FlowUmiParser.FlotanteContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix(@NotNull FlowUmiParser.PostfixContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#entre}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEntre(@NotNull FlowUmiParser.EntreContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#bloque}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloque(@NotNull FlowUmiParser.BloqueContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#sino}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSino(@NotNull FlowUmiParser.SinoContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#tReference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTReference(@NotNull FlowUmiParser.TReferenceContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionLogica}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionLogica(@NotNull FlowUmiParser.ExpresionLogicaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(@NotNull FlowUmiParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#valor}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValor(@NotNull FlowUmiParser.ValorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#enumerador}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEnumerador(@NotNull FlowUmiParser.EnumeradorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#sentencia}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSentencia(@NotNull FlowUmiParser.SentenciaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#bloqueVoid}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloqueVoid(@NotNull FlowUmiParser.BloqueVoidContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionInvocacion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionInvocacion(@NotNull FlowUmiParser.ExpresionInvocacionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#bloquesentencias}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloquesentencias(@NotNull FlowUmiParser.BloquesentenciasContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionRelacional}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionRelacional(@NotNull FlowUmiParser.ExpresionRelacionalContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#bloqueswitch}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBloqueswitch(@NotNull FlowUmiParser.BloqueswitchContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expressionMultiplicativa}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionMultiplicativa(@NotNull FlowUmiParser.ExpressionMultiplicativaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(@NotNull FlowUmiParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#hacerMientras}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHacerMientras(@NotNull FlowUmiParser.HacerMientrasContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresioncondicional}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresioncondicional(@NotNull FlowUmiParser.ExpresioncondicionalContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionPotencia}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionPotencia(@NotNull FlowUmiParser.ExpresionPotenciaContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#decimal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecimal(@NotNull FlowUmiParser.DecimalContext ctx);
	/**
	 * Visit a parse tree produced by {@link FlowUmiParser#expresionAditiva}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpresionAditiva(@NotNull FlowUmiParser.ExpresionAditivaContext ctx);
}