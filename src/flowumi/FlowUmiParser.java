package flowumi;

// Generated from FlowUmi.g4 by ANTLR 4.4
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class FlowUmiParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__41=1, T__40=2, T__39=3, T__38=4, T__37=5, T__36=6, T__35=7, T__34=8, 
		T__33=9, T__32=10, T__31=11, T__30=12, T__29=13, T__28=14, T__27=15, T__26=16, 
		T__25=17, T__24=18, T__23=19, T__22=20, T__21=21, T__20=22, T__19=23, 
		T__18=24, T__17=25, T__16=26, T__15=27, T__14=28, T__13=29, T__12=30, 
		T__11=31, T__10=32, T__9=33, T__8=34, T__7=35, T__6=36, T__5=37, T__4=38, 
		T__3=39, T__2=40, T__1=41, T__0=42, NOMBRE=43, INTEGER=44, CHAR=45, FLOAT=46, 
		PUNTOYCOMA=47, DOSPUNTOS=48, IGUAL=49, LLI=50, LLD=51, CCI=52, CCD=53, 
		PI=54, PD=55, NOT=56, PLUS=57, MINUS=58, MULT=59, DIV=60, MOD=61, POW=62, 
		EQUALS=63, NOTEQUALS=64, LT=65, LTEQ=66, GTEQ=67, GT=68, OR=69, AND=70;
	public static final String[] tokenNames = {
		"<INVALID>", "'decimal'", "'long double'", "'default'", "'inicio'", "'Object'", 
		"'sino'", "'interface'", "'char'", "'return'", "'new'", "'fin'", "'float'", 
		"'class'", "'void'", "'byte'", "'enum'", "'\\u0027'", "'delegate'", "'break'", 
		"'short'", "'double'", "'uint'", "'int'", "'ushort'", "'hacer'", "'para'", 
		"','", "'->'", "'sbyte'", "'ulong'", "'Const'", "'dynamic'", "'Inicio'", 
		"'mientras'", "'si'", "'long'", "'bool'", "'string'", "'caso'", "'\\u0022'", 
		"'Fin'", "'entre'", "NOMBRE", "INTEGER", "CHAR", "FLOAT", "';'", "':'", 
		"'='", "'{'", "'}'", "'['", "']'", "'('", "')'", "NOT", "'+'", "'-'", 
		"'*'", "'/'", "'%'", "'^'", "EQUALS", "NOTEQUALS", "'<'", "'<='", "'>='", 
		"'>'", "OR", "AND"
	};
	public static final int
		RULE_program = 0, RULE_inicio = 1, RULE_fin = 2, RULE_declarar = 3, RULE_sentencia = 4, 
		RULE_para = 5, RULE_hacerMientras = 6, RULE_mientras = 7, RULE_si = 8, 
		RULE_sino = 9, RULE_entre = 10, RULE_bloqueswitchcase = 11, RULE_expresion = 12, 
		RULE_expresionInvocacion = 13, RULE_expresioncondicional = 14, RULE_expresionLogica = 15, 
		RULE_expresionBooleana = 16, RULE_expresionIgualdad = 17, RULE_expresionRelacional = 18, 
		RULE_expresionAditiva = 19, RULE_expressionMultiplicativa = 20, RULE_expresionPotencia = 21, 
		RULE_expresionCerrada = 22, RULE_primaria = 23, RULE_valor = 24, RULE_operacion = 25, 
		RULE_variable = 26, RULE_variablesentencias = 27, RULE_tipo = 28, RULE_tvalor = 29, 
		RULE_simple = 30, RULE_integral = 31, RULE_flotante = 32, RULE_decimal = 33, 
		RULE_booleano = 34, RULE_enumerador = 35, RULE_tReference = 36, RULE_constante = 37, 
		RULE_numero = 38, RULE_bloque = 39, RULE_bloquep = 40, RULE_bloqueswitch = 41, 
		RULE_bloquesentencias = 42, RULE_bloqueVoid = 43, RULE_postfix = 44, RULE_assignment = 45, 
		RULE_forincrement = 46;
	public static final String[] ruleNames = {
		"program", "inicio", "fin", "declarar", "sentencia", "para", "hacerMientras", 
		"mientras", "si", "sino", "entre", "bloqueswitchcase", "expresion", "expresionInvocacion", 
		"expresioncondicional", "expresionLogica", "expresionBooleana", "expresionIgualdad", 
		"expresionRelacional", "expresionAditiva", "expressionMultiplicativa", 
		"expresionPotencia", "expresionCerrada", "primaria", "valor", "operacion", 
		"variable", "variablesentencias", "tipo", "tvalor", "simple", "integral", 
		"flotante", "decimal", "booleano", "enumerador", "tReference", "constante", 
		"numero", "bloque", "bloquep", "bloqueswitch", "bloquesentencias", "bloqueVoid", 
		"postfix", "assignment", "forincrement"
	};

	@Override
	public String getGrammarFileName() { return "FlowUmi.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public FlowUmiParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public DeclararContext declarar(int i) {
			return getRuleContext(DeclararContext.class,i);
		}
		public TerminalNode EOF() { return getToken(FlowUmiParser.EOF, 0); }
		public InicioContext inicio() {
			return getRuleContext(InicioContext.class,0);
		}
		public FinContext fin() {
			return getRuleContext(FinContext.class,0);
		}
		public List<DeclararContext> declarar() {
			return getRuleContexts(DeclararContext.class);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94); inicio();
			setState(98);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__34) | (1L << T__30) | (1L << T__28) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__17) | (1L << T__16) | (1L << T__14) | (1L << T__13) | (1L << T__12) | (1L << T__11) | (1L << T__8) | (1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__0) | (1L << NOMBRE) | (1L << INTEGER) | (1L << FLOAT) | (1L << LLI) | (1L << CCI) | (1L << NOT) | (1L << PLUS) | (1L << MINUS))) != 0)) {
				{
				{
				setState(95); declarar();
				}
				}
				setState(100);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(101); fin();
			setState(102); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InicioContext extends ParserRuleContext {
		public InicioContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inicio; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterInicio(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitInicio(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitInicio(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InicioContext inicio() throws RecognitionException {
		InicioContext _localctx = new InicioContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_inicio);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(104);
			_la = _input.LA(1);
			if ( !(_la==T__38 || _la==T__9) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FinContext extends ParserRuleContext {
		public FinContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fin; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterFin(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitFin(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitFin(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FinContext fin() throws RecognitionException {
		FinContext _localctx = new FinContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_fin);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(106);
			_la = _input.LA(1);
			if ( !(_la==T__31 || _la==T__1) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclararContext extends ParserRuleContext {
		public SentenciaContext sentencia() {
			return getRuleContext(SentenciaContext.class,0);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public OperacionContext operacion() {
			return getRuleContext(OperacionContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public DeclararContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterDeclarar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitDeclarar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitDeclarar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclararContext declarar() throws RecognitionException {
		DeclararContext _localctx = new DeclararContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_declarar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(112);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				{
				setState(108); variable();
				}
				break;
			case 2:
				{
				setState(109); operacion();
				}
				break;
			case 3:
				{
				setState(110); sentencia();
				}
				break;
			case 4:
				{
				setState(111); expresion();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SentenciaContext extends ParserRuleContext {
		public VariablesentenciasContext variablesentencias(int i) {
			return getRuleContext(VariablesentenciasContext.class,i);
		}
		public ParaContext para() {
			return getRuleContext(ParaContext.class,0);
		}
		public TipoContext tipo() {
			return getRuleContext(TipoContext.class,0);
		}
		public TerminalNode NOMBRE() { return getToken(FlowUmiParser.NOMBRE, 0); }
		public List<VariablesentenciasContext> variablesentencias() {
			return getRuleContexts(VariablesentenciasContext.class);
		}
		public EntreContext entre() {
			return getRuleContext(EntreContext.class,0);
		}
		public TerminalNode PUNTOYCOMA() { return getToken(FlowUmiParser.PUNTOYCOMA, 0); }
		public HacerMientrasContext hacerMientras() {
			return getRuleContext(HacerMientrasContext.class,0);
		}
		public BloqueContext bloque() {
			return getRuleContext(BloqueContext.class,0);
		}
		public BloquesentenciasContext bloquesentencias() {
			return getRuleContext(BloquesentenciasContext.class,0);
		}
		public BloqueVoidContext bloqueVoid() {
			return getRuleContext(BloqueVoidContext.class,0);
		}
		public TerminalNode PI() { return getToken(FlowUmiParser.PI, 0); }
		public MientrasContext mientras() {
			return getRuleContext(MientrasContext.class,0);
		}
		public SiContext si() {
			return getRuleContext(SiContext.class,0);
		}
		public TerminalNode PD() { return getToken(FlowUmiParser.PD, 0); }
		public SentenciaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentencia; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterSentencia(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitSentencia(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitSentencia(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SentenciaContext sentencia() throws RecognitionException {
		SentenciaContext _localctx = new SentenciaContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_sentencia);
		int _la;
		try {
			setState(159);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(114); si();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(115); mientras();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(116); entre();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(117); hacerMientras();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(118); para();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(119); tipo();
				setState(120); match(NOMBRE);
				setState(121); bloque();
				setState(122); match(PUNTOYCOMA);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(124); tipo();
				setState(125); match(NOMBRE);
				setState(126); match(PI);
				setState(136);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__34) | (1L << T__30) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__13) | (1L << T__12) | (1L << T__11) | (1L << T__6) | (1L << T__5))) != 0)) {
					{
					{
					setState(127); variablesentencias();
					setState(131);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__15) {
						{
						{
						setState(128); match(T__15);
						}
						}
						setState(133);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					}
					setState(138);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(139); match(PD);
				setState(140); bloquesentencias();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(142); match(T__28);
				setState(143); match(NOMBRE);
				setState(144); match(PI);
				setState(154);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__34) | (1L << T__30) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__13) | (1L << T__12) | (1L << T__11) | (1L << T__6) | (1L << T__5))) != 0)) {
					{
					{
					setState(145); variablesentencias();
					setState(149);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__15) {
						{
						{
						setState(146); match(T__15);
						}
						}
						setState(151);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					}
					setState(156);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(157); match(PD);
				setState(158); bloqueVoid();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParaContext extends ParserRuleContext {
		public TerminalNode NOMBRE(int i) {
			return getToken(FlowUmiParser.NOMBRE, i);
		}
		public TerminalNode PI() { return getToken(FlowUmiParser.PI, 0); }
		public TerminalNode INTEGER() { return getToken(FlowUmiParser.INTEGER, 0); }
		public List<TerminalNode> NOMBRE() { return getTokens(FlowUmiParser.NOMBRE); }
		public ForincrementContext forincrement() {
			return getRuleContext(ForincrementContext.class,0);
		}
		public TerminalNode PUNTOYCOMA(int i) {
			return getToken(FlowUmiParser.PUNTOYCOMA, i);
		}
		public BloquepContext bloquep() {
			return getRuleContext(BloquepContext.class,0);
		}
		public TerminalNode PD() { return getToken(FlowUmiParser.PD, 0); }
		public List<TerminalNode> PUNTOYCOMA() { return getTokens(FlowUmiParser.PUNTOYCOMA); }
		public ExpresioncondicionalContext expresioncondicional() {
			return getRuleContext(ExpresioncondicionalContext.class,0);
		}
		public ParaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_para; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterPara(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitPara(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitPara(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParaContext para() throws RecognitionException {
		ParaContext _localctx = new ParaContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_para);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(161); match(T__16);
			setState(162); match(PI);
			setState(171);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				{
				setState(163); match(T__19);
				setState(164); match(NOMBRE);
				setState(165); match(IGUAL);
				setState(166);
				_la = _input.LA(1);
				if ( !(_la==NOMBRE || _la==INTEGER) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			case 2:
				{
				setState(167); match(NOMBRE);
				}
				break;
			case 3:
				{
				setState(168); match(NOMBRE);
				setState(169); match(IGUAL);
				setState(170);
				_la = _input.LA(1);
				if ( !(_la==NOMBRE || _la==INTEGER) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;
			}
			setState(173); match(PUNTOYCOMA);
			setState(174); expresioncondicional();
			setState(175); match(PUNTOYCOMA);
			setState(176); match(NOMBRE);
			setState(177); forincrement();
			setState(178); match(PD);
			setState(179); bloquep();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HacerMientrasContext extends ParserRuleContext {
		public TerminalNode PI() { return getToken(FlowUmiParser.PI, 0); }
		public BloquepContext bloquep() {
			return getRuleContext(BloquepContext.class,0);
		}
		public TerminalNode PD() { return getToken(FlowUmiParser.PD, 0); }
		public TerminalNode PUNTOYCOMA() { return getToken(FlowUmiParser.PUNTOYCOMA, 0); }
		public ExpresioncondicionalContext expresioncondicional() {
			return getRuleContext(ExpresioncondicionalContext.class,0);
		}
		public HacerMientrasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_hacerMientras; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterHacerMientras(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitHacerMientras(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitHacerMientras(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HacerMientrasContext hacerMientras() throws RecognitionException {
		HacerMientrasContext _localctx = new HacerMientrasContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_hacerMientras);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(181); match(T__17);
			setState(182); bloquep();
			setState(183); match(T__8);
			setState(184); match(PI);
			setState(185); expresioncondicional();
			setState(186); match(PD);
			setState(187); match(PUNTOYCOMA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MientrasContext extends ParserRuleContext {
		public TerminalNode PI() { return getToken(FlowUmiParser.PI, 0); }
		public BloquepContext bloquep() {
			return getRuleContext(BloquepContext.class,0);
		}
		public ExpresioncondicionalContext expresioncondicional(int i) {
			return getRuleContext(ExpresioncondicionalContext.class,i);
		}
		public TerminalNode PD() { return getToken(FlowUmiParser.PD, 0); }
		public List<ExpresioncondicionalContext> expresioncondicional() {
			return getRuleContexts(ExpresioncondicionalContext.class);
		}
		public MientrasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mientras; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterMientras(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitMientras(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitMientras(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MientrasContext mientras() throws RecognitionException {
		MientrasContext _localctx = new MientrasContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_mientras);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(189); match(T__8);
			setState(190); match(PI);
			setState(194);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOMBRE) | (1L << INTEGER) | (1L << FLOAT) | (1L << NOT) | (1L << MINUS))) != 0)) {
				{
				{
				setState(191); expresioncondicional();
				}
				}
				setState(196);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(197); match(PD);
			setState(198); bloquep();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SiContext extends ParserRuleContext {
		public TerminalNode PI() { return getToken(FlowUmiParser.PI, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public SinoContext sino(int i) {
			return getRuleContext(SinoContext.class,i);
		}
		public List<SinoContext> sino() {
			return getRuleContexts(SinoContext.class);
		}
		public BloquepContext bloquep() {
			return getRuleContext(BloquepContext.class,0);
		}
		public TerminalNode PD() { return getToken(FlowUmiParser.PD, 0); }
		public SiContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_si; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterSi(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitSi(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitSi(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SiContext si() throws RecognitionException {
		SiContext _localctx = new SiContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_si);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200); match(T__7);
			setState(201); match(PI);
			setState(202); expresion();
			setState(203); match(PD);
			setState(204); bloquep();
			setState(208);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__36) {
				{
				{
				setState(205); sino();
				}
				}
				setState(210);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SinoContext extends ParserRuleContext {
		public BloquepContext bloquep() {
			return getRuleContext(BloquepContext.class,0);
		}
		public SinoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sino; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterSino(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitSino(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitSino(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SinoContext sino() throws RecognitionException {
		SinoContext _localctx = new SinoContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_sino);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(211); match(T__36);
			setState(212); bloquep();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntreContext extends ParserRuleContext {
		public TerminalNode PI() { return getToken(FlowUmiParser.PI, 0); }
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public TerminalNode PD() { return getToken(FlowUmiParser.PD, 0); }
		public BloqueswitchcaseContext bloqueswitchcase() {
			return getRuleContext(BloqueswitchcaseContext.class,0);
		}
		public EntreContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entre; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterEntre(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitEntre(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitEntre(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EntreContext entre() throws RecognitionException {
		EntreContext _localctx = new EntreContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_entre);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214); match(T__0);
			setState(215); match(PI);
			setState(216); expresion();
			setState(217); match(PD);
			setState(218); bloqueswitchcase();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueswitchcaseContext extends ParserRuleContext {
		public BloqueswitchContext bloqueswitch(int i) {
			return getRuleContext(BloqueswitchContext.class,i);
		}
		public TerminalNode NOMBRE(int i) {
			return getToken(FlowUmiParser.NOMBRE, i);
		}
		public TerminalNode LLI() { return getToken(FlowUmiParser.LLI, 0); }
		public List<TerminalNode> NOMBRE() { return getTokens(FlowUmiParser.NOMBRE); }
		public TerminalNode DOSPUNTOS(int i) {
			return getToken(FlowUmiParser.DOSPUNTOS, i);
		}
		public List<BloqueswitchContext> bloqueswitch() {
			return getRuleContexts(BloqueswitchContext.class);
		}
		public TerminalNode LLD() { return getToken(FlowUmiParser.LLD, 0); }
		public List<TerminalNode> DOSPUNTOS() { return getTokens(FlowUmiParser.DOSPUNTOS); }
		public BloqueswitchcaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloqueswitchcase; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterBloqueswitchcase(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitBloqueswitchcase(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitBloqueswitchcase(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueswitchcaseContext bloqueswitchcase() throws RecognitionException {
		BloqueswitchcaseContext _localctx = new BloqueswitchcaseContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_bloqueswitchcase);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(220); match(LLI);
			setState(225); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(221); match(T__3);
				setState(222); match(NOMBRE);
				setState(223); match(DOSPUNTOS);
				setState(224); bloqueswitch();
				}
				}
				setState(227); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__3 );
			setState(234);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__39) {
				{
				{
				setState(229); match(T__39);
				setState(230); match(DOSPUNTOS);
				setState(231); bloqueswitch();
				}
				}
				setState(236);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(237); match(LLD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionContext extends ParserRuleContext {
		public ExpresionInvocacionContext expresionInvocacion() {
			return getRuleContext(ExpresionInvocacionContext.class,0);
		}
		public ExpresionAditivaContext expresionAditiva() {
			return getRuleContext(ExpresionAditivaContext.class,0);
		}
		public ExpresioncondicionalContext expresioncondicional() {
			return getRuleContext(ExpresioncondicionalContext.class,0);
		}
		public ExpresionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionContext expresion() throws RecognitionException {
		ExpresionContext _localctx = new ExpresionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_expresion);
		try {
			setState(242);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(239); expresioncondicional();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(240); expresionAditiva();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(241); expresionInvocacion();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionInvocacionContext extends ParserRuleContext {
		public TerminalNode NOMBRE(int i) {
			return getToken(FlowUmiParser.NOMBRE, i);
		}
		public TerminalNode PI() { return getToken(FlowUmiParser.PI, 0); }
		public List<ValorContext> valor() {
			return getRuleContexts(ValorContext.class);
		}
		public List<TerminalNode> NOMBRE() { return getTokens(FlowUmiParser.NOMBRE); }
		public List<TipoContext> tipo() {
			return getRuleContexts(TipoContext.class);
		}
		public TipoContext tipo(int i) {
			return getRuleContext(TipoContext.class,i);
		}
		public ValorContext valor(int i) {
			return getRuleContext(ValorContext.class,i);
		}
		public TerminalNode PD() { return getToken(FlowUmiParser.PD, 0); }
		public TerminalNode PUNTOYCOMA() { return getToken(FlowUmiParser.PUNTOYCOMA, 0); }
		public ExpresionInvocacionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionInvocacion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionInvocacion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionInvocacion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionInvocacion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionInvocacionContext expresionInvocacion() throws RecognitionException {
		ExpresionInvocacionContext _localctx = new ExpresionInvocacionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_expresionInvocacion);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(244); match(NOMBRE);
			setState(245); match(PI);
			setState(253);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__30) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__13) | (1L << T__12) | (1L << T__6) | (1L << T__5))) != 0)) {
				{
				{
				setState(246); tipo();
				setState(249);
				switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
				case 1:
					{
					setState(247); match(NOMBRE);
					}
					break;
				case 2:
					{
					setState(248); valor();
					}
					break;
				}
				}
				}
				setState(255);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(256); match(PD);
			setState(257); match(PUNTOYCOMA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresioncondicionalContext extends ParserRuleContext {
		public ExpresionLogicaContext expresionLogica() {
			return getRuleContext(ExpresionLogicaContext.class,0);
		}
		public ExpresioncondicionalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresioncondicional; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresioncondicional(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresioncondicional(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresioncondicional(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresioncondicionalContext expresioncondicional() throws RecognitionException {
		ExpresioncondicionalContext _localctx = new ExpresioncondicionalContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_expresioncondicional);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(259); expresionLogica();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionLogicaContext extends ParserRuleContext {
		public List<ExpresionBooleanaContext> expresionBooleana() {
			return getRuleContexts(ExpresionBooleanaContext.class);
		}
		public ExpresionBooleanaContext expresionBooleana(int i) {
			return getRuleContext(ExpresionBooleanaContext.class,i);
		}
		public List<TerminalNode> OR() { return getTokens(FlowUmiParser.OR); }
		public TerminalNode OR(int i) {
			return getToken(FlowUmiParser.OR, i);
		}
		public ExpresionLogicaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionLogica; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionLogica(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionLogica(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionLogica(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionLogicaContext expresionLogica() throws RecognitionException {
		ExpresionLogicaContext _localctx = new ExpresionLogicaContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_expresionLogica);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261); expresionBooleana();
			setState(266);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OR) {
				{
				{
				setState(262); match(OR);
				setState(263); expresionBooleana();
				}
				}
				setState(268);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionBooleanaContext extends ParserRuleContext {
		public List<TerminalNode> EQUALS() { return getTokens(FlowUmiParser.EQUALS); }
		public TerminalNode NOTEQUALS(int i) {
			return getToken(FlowUmiParser.NOTEQUALS, i);
		}
		public List<TerminalNode> NOTEQUALS() { return getTokens(FlowUmiParser.NOTEQUALS); }
		public ExpresionIgualdadContext expresionIgualdad(int i) {
			return getRuleContext(ExpresionIgualdadContext.class,i);
		}
		public TerminalNode EQUALS(int i) {
			return getToken(FlowUmiParser.EQUALS, i);
		}
		public List<ExpresionIgualdadContext> expresionIgualdad() {
			return getRuleContexts(ExpresionIgualdadContext.class);
		}
		public ExpresionBooleanaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionBooleana; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionBooleana(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionBooleana(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionBooleana(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionBooleanaContext expresionBooleana() throws RecognitionException {
		ExpresionBooleanaContext _localctx = new ExpresionBooleanaContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_expresionBooleana);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(269); expresionIgualdad();
			setState(274);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==EQUALS || _la==NOTEQUALS) {
				{
				{
				setState(270);
				_la = _input.LA(1);
				if ( !(_la==EQUALS || _la==NOTEQUALS) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(271); expresionIgualdad();
				}
				}
				setState(276);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionIgualdadContext extends ParserRuleContext {
		public TerminalNode AND(int i) {
			return getToken(FlowUmiParser.AND, i);
		}
		public List<TerminalNode> AND() { return getTokens(FlowUmiParser.AND); }
		public List<ExpresionRelacionalContext> expresionRelacional() {
			return getRuleContexts(ExpresionRelacionalContext.class);
		}
		public ExpresionRelacionalContext expresionRelacional(int i) {
			return getRuleContext(ExpresionRelacionalContext.class,i);
		}
		public ExpresionIgualdadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionIgualdad; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionIgualdad(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionIgualdad(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionIgualdad(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionIgualdadContext expresionIgualdad() throws RecognitionException {
		ExpresionIgualdadContext _localctx = new ExpresionIgualdadContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_expresionIgualdad);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(277); expresionRelacional();
			setState(282);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AND) {
				{
				{
				setState(278); match(AND);
				setState(279); expresionRelacional();
				}
				}
				setState(284);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionRelacionalContext extends ParserRuleContext {
		public TerminalNode LTEQ(int i) {
			return getToken(FlowUmiParser.LTEQ, i);
		}
		public ExpresionAditivaContext expresionAditiva(int i) {
			return getRuleContext(ExpresionAditivaContext.class,i);
		}
		public TerminalNode GTEQ(int i) {
			return getToken(FlowUmiParser.GTEQ, i);
		}
		public List<TerminalNode> LTEQ() { return getTokens(FlowUmiParser.LTEQ); }
		public List<TerminalNode> LT() { return getTokens(FlowUmiParser.LT); }
		public List<TerminalNode> GT() { return getTokens(FlowUmiParser.GT); }
		public List<ExpresionAditivaContext> expresionAditiva() {
			return getRuleContexts(ExpresionAditivaContext.class);
		}
		public TerminalNode GT(int i) {
			return getToken(FlowUmiParser.GT, i);
		}
		public List<TerminalNode> GTEQ() { return getTokens(FlowUmiParser.GTEQ); }
		public TerminalNode LT(int i) {
			return getToken(FlowUmiParser.LT, i);
		}
		public ExpresionRelacionalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionRelacional; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionRelacional(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionRelacional(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionRelacional(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionRelacionalContext expresionRelacional() throws RecognitionException {
		ExpresionRelacionalContext _localctx = new ExpresionRelacionalContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_expresionRelacional);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(285); expresionAditiva();
			setState(290);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (LT - 65)) | (1L << (LTEQ - 65)) | (1L << (GTEQ - 65)) | (1L << (GT - 65)))) != 0)) {
				{
				{
				setState(286);
				_la = _input.LA(1);
				if ( !(((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (LT - 65)) | (1L << (LTEQ - 65)) | (1L << (GTEQ - 65)) | (1L << (GT - 65)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(287); expresionAditiva();
				}
				}
				setState(292);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionAditivaContext extends ParserRuleContext {
		public TerminalNode MINUS(int i) {
			return getToken(FlowUmiParser.MINUS, i);
		}
		public ExpressionMultiplicativaContext expressionMultiplicativa(int i) {
			return getRuleContext(ExpressionMultiplicativaContext.class,i);
		}
		public List<ExpressionMultiplicativaContext> expressionMultiplicativa() {
			return getRuleContexts(ExpressionMultiplicativaContext.class);
		}
		public List<TerminalNode> PLUS() { return getTokens(FlowUmiParser.PLUS); }
		public List<TerminalNode> MINUS() { return getTokens(FlowUmiParser.MINUS); }
		public TerminalNode PLUS(int i) {
			return getToken(FlowUmiParser.PLUS, i);
		}
		public ExpresionAditivaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionAditiva; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionAditiva(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionAditiva(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionAditiva(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionAditivaContext expresionAditiva() throws RecognitionException {
		ExpresionAditivaContext _localctx = new ExpresionAditivaContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_expresionAditiva);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(293); expressionMultiplicativa();
			setState(298);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(294);
					_la = _input.LA(1);
					if ( !(_la==PLUS || _la==MINUS) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					setState(295); expressionMultiplicativa();
					}
					} 
				}
				setState(300);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionMultiplicativaContext extends ParserRuleContext {
		public TerminalNode MULT(int i) {
			return getToken(FlowUmiParser.MULT, i);
		}
		public List<TerminalNode> MULT() { return getTokens(FlowUmiParser.MULT); }
		public ExpresionPotenciaContext expresionPotencia(int i) {
			return getRuleContext(ExpresionPotenciaContext.class,i);
		}
		public List<ExpresionPotenciaContext> expresionPotencia() {
			return getRuleContexts(ExpresionPotenciaContext.class);
		}
		public List<TerminalNode> MOD() { return getTokens(FlowUmiParser.MOD); }
		public List<TerminalNode> DIV() { return getTokens(FlowUmiParser.DIV); }
		public TerminalNode DIV(int i) {
			return getToken(FlowUmiParser.DIV, i);
		}
		public TerminalNode MOD(int i) {
			return getToken(FlowUmiParser.MOD, i);
		}
		public ExpressionMultiplicativaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionMultiplicativa; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpressionMultiplicativa(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpressionMultiplicativa(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpressionMultiplicativa(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionMultiplicativaContext expressionMultiplicativa() throws RecognitionException {
		ExpressionMultiplicativaContext _localctx = new ExpressionMultiplicativaContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_expressionMultiplicativa);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(301); expresionPotencia();
			setState(306);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULT) | (1L << DIV) | (1L << MOD))) != 0)) {
				{
				{
				setState(302);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULT) | (1L << DIV) | (1L << MOD))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				setState(303); expresionPotencia();
				}
				}
				setState(308);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionPotenciaContext extends ParserRuleContext {
		public List<ExpresionCerradaContext> expresionCerrada() {
			return getRuleContexts(ExpresionCerradaContext.class);
		}
		public TerminalNode POW(int i) {
			return getToken(FlowUmiParser.POW, i);
		}
		public ExpresionCerradaContext expresionCerrada(int i) {
			return getRuleContext(ExpresionCerradaContext.class,i);
		}
		public List<TerminalNode> POW() { return getTokens(FlowUmiParser.POW); }
		public ExpresionPotenciaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionPotencia; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionPotencia(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionPotencia(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionPotencia(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionPotenciaContext expresionPotencia() throws RecognitionException {
		ExpresionPotenciaContext _localctx = new ExpresionPotenciaContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_expresionPotencia);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(309); expresionCerrada();
			setState(314);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==POW) {
				{
				{
				setState(310); match(POW);
				setState(311); expresionCerrada();
				}
				}
				setState(316);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresionCerradaContext extends ParserRuleContext {
		public TerminalNode NOT() { return getToken(FlowUmiParser.NOT, 0); }
		public ExpresionCerradaContext expresionCerrada() {
			return getRuleContext(ExpresionCerradaContext.class,0);
		}
		public TerminalNode MINUS() { return getToken(FlowUmiParser.MINUS, 0); }
		public PrimariaContext primaria() {
			return getRuleContext(PrimariaContext.class,0);
		}
		public ExpresionCerradaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expresionCerrada; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterExpresionCerrada(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitExpresionCerrada(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitExpresionCerrada(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresionCerradaContext expresionCerrada() throws RecognitionException {
		ExpresionCerradaContext _localctx = new ExpresionCerradaContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_expresionCerrada);
		try {
			setState(322);
			switch (_input.LA(1)) {
			case NOMBRE:
			case INTEGER:
			case FLOAT:
				enterOuterAlt(_localctx, 1);
				{
				setState(317); primaria();
				}
				break;
			case NOT:
				enterOuterAlt(_localctx, 2);
				{
				setState(318); match(NOT);
				setState(319); primaria();
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 3);
				{
				setState(320); match(MINUS);
				setState(321); expresionCerrada();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimariaContext extends ParserRuleContext {
		public ValorContext valor() {
			return getRuleContext(ValorContext.class,0);
		}
		public PrimariaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaria; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterPrimaria(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitPrimaria(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitPrimaria(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimariaContext primaria() throws RecognitionException {
		PrimariaContext _localctx = new PrimariaContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_primaria);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(324); valor();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValorContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(FlowUmiParser.INTEGER, 0); }
		public TerminalNode NOMBRE() { return getToken(FlowUmiParser.NOMBRE, 0); }
		public TerminalNode FLOAT() { return getToken(FlowUmiParser.FLOAT, 0); }
		public ValorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterValor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitValor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitValor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValorContext valor() throws RecognitionException {
		ValorContext _localctx = new ValorContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_valor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(326);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NOMBRE) | (1L << INTEGER) | (1L << FLOAT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperacionContext extends ParserRuleContext {
		public TerminalNode NOMBRE(int i) {
			return getToken(FlowUmiParser.NOMBRE, i);
		}
		public ExpresionContext expresion() {
			return getRuleContext(ExpresionContext.class,0);
		}
		public TerminalNode CCD() { return getToken(FlowUmiParser.CCD, 0); }
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public List<TerminalNode> NOMBRE() { return getTokens(FlowUmiParser.NOMBRE); }
		public TerminalNode CCI() { return getToken(FlowUmiParser.CCI, 0); }
		public List<NumeroContext> numero() {
			return getRuleContexts(NumeroContext.class);
		}
		public NumeroContext numero(int i) {
			return getRuleContext(NumeroContext.class,i);
		}
		public PostfixContext postfix() {
			return getRuleContext(PostfixContext.class,0);
		}
		public TerminalNode IGUAL() { return getToken(FlowUmiParser.IGUAL, 0); }
		public TerminalNode PUNTOYCOMA() { return getToken(FlowUmiParser.PUNTOYCOMA, 0); }
		public OperacionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operacion; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterOperacion(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitOperacion(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitOperacion(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperacionContext operacion() throws RecognitionException {
		OperacionContext _localctx = new OperacionContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_operacion);
		try {
			setState(362);
			switch ( getInterpreter().adaptivePredict(_input,26,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(328); match(NOMBRE);
				setState(329); match(IGUAL);
				setState(330); expresion();
				setState(331); match(PUNTOYCOMA);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(333); match(NOMBRE);
				setState(334); match(CCI);
				setState(337);
				switch (_input.LA(1)) {
				case INTEGER:
				case FLOAT:
					{
					setState(335); numero();
					}
					break;
				case NOMBRE:
					{
					setState(336); match(NOMBRE);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(339); match(CCD);
				setState(340); match(IGUAL);
				setState(343);
				switch (_input.LA(1)) {
				case INTEGER:
				case FLOAT:
					{
					setState(341); numero();
					}
					break;
				case NOMBRE:
					{
					setState(342); match(NOMBRE);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(345); match(PUNTOYCOMA);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(346); postfix();
				setState(347); match(NOMBRE);
				setState(348); match(PUNTOYCOMA);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(350); match(NOMBRE);
				setState(351); postfix();
				setState(352); match(PUNTOYCOMA);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(354); match(NOMBRE);
				setState(355); assignment();
				setState(358);
				switch (_input.LA(1)) {
				case NOMBRE:
					{
					setState(356); match(NOMBRE);
					}
					break;
				case INTEGER:
				case FLOAT:
					{
					setState(357); numero();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(360); match(PUNTOYCOMA);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public SimpleContext simple() {
			return getRuleContext(SimpleContext.class,0);
		}
		public List<TipoContext> tipo() {
			return getRuleContexts(TipoContext.class);
		}
		public List<TerminalNode> NOMBRE() { return getTokens(FlowUmiParser.NOMBRE); }
		public List<NumeroContext> numero() {
			return getRuleContexts(NumeroContext.class);
		}
		public List<TerminalNode> CCI() { return getTokens(FlowUmiParser.CCI); }
		public TerminalNode PUNTOYCOMA() { return getToken(FlowUmiParser.PUNTOYCOMA, 0); }
		public TerminalNode IGUAL() { return getToken(FlowUmiParser.IGUAL, 0); }
		public BloqueContext bloque() {
			return getRuleContext(BloqueContext.class,0);
		}
		public TReferenceContext tReference() {
			return getRuleContext(TReferenceContext.class,0);
		}
		public TerminalNode CCD(int i) {
			return getToken(FlowUmiParser.CCD, i);
		}
		public TerminalNode NOMBRE(int i) {
			return getToken(FlowUmiParser.NOMBRE, i);
		}
		public List<TerminalNode> CCD() { return getTokens(FlowUmiParser.CCD); }
		public ConstanteContext constante() {
			return getRuleContext(ConstanteContext.class,0);
		}
		public TipoContext tipo(int i) {
			return getRuleContext(TipoContext.class,i);
		}
		public NumeroContext numero(int i) {
			return getRuleContext(NumeroContext.class,i);
		}
		public TerminalNode CCI(int i) {
			return getToken(FlowUmiParser.CCI, i);
		}
		public TerminalNode CHAR() { return getToken(FlowUmiParser.CHAR, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_variable);
		int _la;
		try {
			setState(502);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(364); constante();
				setState(365); tipo();
				setState(366); match(NOMBRE);
				setState(367); match(PUNTOYCOMA);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(369); constante();
				setState(370); simple();
				setState(371); match(NOMBRE);
				setState(372); match(IGUAL);
				setState(373); numero();
				setState(374); match(PUNTOYCOMA);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(376); constante();
				setState(377); tReference();
				setState(378); match(NOMBRE);
				setState(379); match(IGUAL);
				setState(384);
				switch (_input.LA(1)) {
				case NOMBRE:
					{
					setState(380); match(NOMBRE);
					}
					break;
				case T__2:
					{
					setState(381); match(T__2);
					setState(382); match(NOMBRE);
					setState(383); match(T__2);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(386); match(PUNTOYCOMA);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(388); tipo();
				setState(389); match(NOMBRE);
				setState(390); bloque();
				setState(391); match(PUNTOYCOMA);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(393); tipo();
				setState(394); match(NOMBRE);
				setState(395); match(PUNTOYCOMA);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(397); tipo();
				setState(398); match(NOMBRE);
				setState(403);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(399); match(T__15);
					setState(400); match(NOMBRE);
					}
					}
					setState(405);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(406); match(PUNTOYCOMA);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(408); tipo();
				setState(409); match(NOMBRE);
				setState(414);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(410); match(T__15);
					setState(411); match(NOMBRE);
					}
					}
					setState(416);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(417); match(IGUAL);
				setState(418); match(NOMBRE);
				setState(423);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(419); match(T__15);
					setState(420); match(NOMBRE);
					}
					}
					setState(425);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(426); match(PUNTOYCOMA);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(428); tipo();
				setState(429); match(NOMBRE);
				setState(434);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(430); match(T__15);
					setState(431); match(NOMBRE);
					}
					}
					setState(436);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(437); match(IGUAL);
				setState(438); numero();
				setState(443);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(439); match(T__15);
					setState(440); numero();
					}
					}
					setState(445);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(446); match(PUNTOYCOMA);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(448); tipo();
				setState(449); match(NOMBRE);
				setState(450); match(IGUAL);
				setState(451); match(NOMBRE);
				setState(452); match(PUNTOYCOMA);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(454); tipo();
				setState(455); match(NOMBRE);
				setState(456); match(IGUAL);
				setState(457); numero();
				setState(458); match(PUNTOYCOMA);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(460); match(T__34);
				setState(461); match(NOMBRE);
				setState(462); match(IGUAL);
				setState(463); match(T__25);
				setState(464); match(CHAR);
				setState(465); match(T__25);
				setState(466); match(PUNTOYCOMA);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(467); match(T__34);
				setState(468); match(CCI);
				setState(469); match(CCD);
				setState(470); match(NOMBRE);
				setState(471); match(IGUAL);
				setState(472); match(T__32);
				setState(473); match(T__34);
				setState(474); match(CCI);
				{
				setState(475); numero();
				}
				setState(476); match(CCD);
				setState(477); match(PUNTOYCOMA);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(479); tipo();
				setState(480); match(CCI);
				setState(481); match(CCD);
				setState(482); match(NOMBRE);
				setState(483); match(IGUAL);
				setState(484); match(T__32);
				setState(485); tipo();
				setState(486); match(CCI);
				setState(489);
				switch (_input.LA(1)) {
				case INTEGER:
				case FLOAT:
					{
					setState(487); numero();
					}
					break;
				case NOMBRE:
					{
					setState(488); match(NOMBRE);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(491); match(CCD);
				setState(492); match(PUNTOYCOMA);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(494); tipo();
				setState(495); match(CCI);
				setState(496); match(CCD);
				setState(497); match(NOMBRE);
				setState(498); match(IGUAL);
				setState(499); bloque();
				setState(500); match(PUNTOYCOMA);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariablesentenciasContext extends ParserRuleContext {
		public SimpleContext simple() {
			return getRuleContext(SimpleContext.class,0);
		}
		public TipoContext tipo() {
			return getRuleContext(TipoContext.class,0);
		}
		public List<TerminalNode> NOMBRE() { return getTokens(FlowUmiParser.NOMBRE); }
		public NumeroContext numero() {
			return getRuleContext(NumeroContext.class,0);
		}
		public List<TerminalNode> CCI() { return getTokens(FlowUmiParser.CCI); }
		public TerminalNode IGUAL() { return getToken(FlowUmiParser.IGUAL, 0); }
		public TReferenceContext tReference() {
			return getRuleContext(TReferenceContext.class,0);
		}
		public TerminalNode CCD(int i) {
			return getToken(FlowUmiParser.CCD, i);
		}
		public TerminalNode NOMBRE(int i) {
			return getToken(FlowUmiParser.NOMBRE, i);
		}
		public List<TerminalNode> CCD() { return getTokens(FlowUmiParser.CCD); }
		public ConstanteContext constante() {
			return getRuleContext(ConstanteContext.class,0);
		}
		public TerminalNode CCI(int i) {
			return getToken(FlowUmiParser.CCI, i);
		}
		public TerminalNode CHAR() { return getToken(FlowUmiParser.CHAR, 0); }
		public VariablesentenciasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variablesentencias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterVariablesentencias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitVariablesentencias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitVariablesentencias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariablesentenciasContext variablesentencias() throws RecognitionException {
		VariablesentenciasContext _localctx = new VariablesentenciasContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_variablesentencias);
		try {
			setState(554);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(504); constante();
				setState(505); tipo();
				setState(506); match(NOMBRE);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(508); constante();
				setState(509); simple();
				setState(510); match(NOMBRE);
				setState(511); match(IGUAL);
				setState(512); numero();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(514); constante();
				setState(515); tReference();
				setState(516); match(NOMBRE);
				setState(517); match(IGUAL);
				setState(522);
				switch (_input.LA(1)) {
				case NOMBRE:
					{
					setState(518); match(NOMBRE);
					}
					break;
				case T__2:
					{
					setState(519); match(T__2);
					setState(520); match(NOMBRE);
					setState(521); match(T__2);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(524); tipo();
				setState(525); match(NOMBRE);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(527); tipo();
				setState(528); match(NOMBRE);
				setState(529); match(IGUAL);
				setState(530); match(NOMBRE);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(532); tipo();
				setState(533); match(NOMBRE);
				setState(534); match(IGUAL);
				setState(535); numero();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(537); match(T__34);
				setState(538); match(NOMBRE);
				setState(539); match(IGUAL);
				setState(540); match(T__25);
				setState(541); match(CHAR);
				setState(542); match(T__25);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(543); match(T__34);
				setState(544); match(CCI);
				setState(545); match(CCD);
				setState(546); match(NOMBRE);
				setState(547); match(IGUAL);
				setState(548); match(T__32);
				setState(549); match(T__34);
				setState(550); match(CCI);
				{
				setState(551); numero();
				}
				setState(552); match(CCD);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TipoContext extends ParserRuleContext {
		public TvalorContext tvalor() {
			return getRuleContext(TvalorContext.class,0);
		}
		public TipoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tipo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterTipo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitTipo(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitTipo(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TipoContext tipo() throws RecognitionException {
		TipoContext _localctx = new TipoContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_tipo);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(556); tvalor();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TvalorContext extends ParserRuleContext {
		public SimpleContext simple() {
			return getRuleContext(SimpleContext.class,0);
		}
		public TvalorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tvalor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterTvalor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitTvalor(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitTvalor(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TvalorContext tvalor() throws RecognitionException {
		TvalorContext _localctx = new TvalorContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_tvalor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(558); simple();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleContext extends ParserRuleContext {
		public IntegralContext integral() {
			return getRuleContext(IntegralContext.class,0);
		}
		public DecimalContext decimal() {
			return getRuleContext(DecimalContext.class,0);
		}
		public FlotanteContext flotante() {
			return getRuleContext(FlotanteContext.class,0);
		}
		public BooleanoContext booleano() {
			return getRuleContext(BooleanoContext.class,0);
		}
		public SimpleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simple; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterSimple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitSimple(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitSimple(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleContext simple() throws RecognitionException {
		SimpleContext _localctx = new SimpleContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_simple);
		try {
			setState(564);
			switch (_input.LA(1)) {
			case T__27:
			case T__22:
			case T__20:
			case T__19:
			case T__18:
			case T__13:
			case T__12:
			case T__6:
				enterOuterAlt(_localctx, 1);
				{
				setState(560); integral();
				}
				break;
			case T__40:
			case T__30:
			case T__21:
				enterOuterAlt(_localctx, 2);
				{
				setState(561); flotante();
				}
				break;
			case T__41:
				enterOuterAlt(_localctx, 3);
				{
				setState(562); decimal();
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 4);
				{
				setState(563); booleano();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegralContext extends ParserRuleContext {
		public IntegralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterIntegral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitIntegral(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitIntegral(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntegralContext integral() throws RecognitionException {
		IntegralContext _localctx = new IntegralContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_integral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(566);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__27) | (1L << T__22) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__13) | (1L << T__12) | (1L << T__6))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FlotanteContext extends ParserRuleContext {
		public FlotanteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_flotante; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterFlotante(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitFlotante(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitFlotante(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FlotanteContext flotante() throws RecognitionException {
		FlotanteContext _localctx = new FlotanteContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_flotante);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(568);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__40) | (1L << T__30) | (1L << T__21))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DecimalContext extends ParserRuleContext {
		public DecimalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decimal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterDecimal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitDecimal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitDecimal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DecimalContext decimal() throws RecognitionException {
		DecimalContext _localctx = new DecimalContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_decimal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(570); match(T__41);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanoContext extends ParserRuleContext {
		public BooleanoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleano; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterBooleano(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitBooleano(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitBooleano(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BooleanoContext booleano() throws RecognitionException {
		BooleanoContext _localctx = new BooleanoContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_booleano);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(572); match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnumeradorContext extends ParserRuleContext {
		public EnumeradorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumerador; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterEnumerador(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitEnumerador(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitEnumerador(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EnumeradorContext enumerador() throws RecognitionException {
		EnumeradorContext _localctx = new EnumeradorContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_enumerador);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(574); match(T__26);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TReferenceContext extends ParserRuleContext {
		public TReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tReference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterTReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitTReference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitTReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TReferenceContext tReference() throws RecognitionException {
		TReferenceContext _localctx = new TReferenceContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_tReference);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(576);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__37) | (1L << T__35) | (1L << T__29) | (1L << T__24) | (1L << T__10) | (1L << T__4))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstanteContext extends ParserRuleContext {
		public ConstanteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constante; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterConstante(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitConstante(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitConstante(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstanteContext constante() throws RecognitionException {
		ConstanteContext _localctx = new ConstanteContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_constante);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(578); match(T__11);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumeroContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(FlowUmiParser.INTEGER, 0); }
		public TerminalNode FLOAT() { return getToken(FlowUmiParser.FLOAT, 0); }
		public NumeroContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numero; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterNumero(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitNumero(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitNumero(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumeroContext numero() throws RecognitionException {
		NumeroContext _localctx = new NumeroContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_numero);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(580);
			_la = _input.LA(1);
			if ( !(_la==INTEGER || _la==FLOAT) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueContext extends ParserRuleContext {
		public TerminalNode NOMBRE(int i) {
			return getToken(FlowUmiParser.NOMBRE, i);
		}
		public TerminalNode LLI() { return getToken(FlowUmiParser.LLI, 0); }
		public List<TerminalNode> NOMBRE() { return getTokens(FlowUmiParser.NOMBRE); }
		public List<NumeroContext> numero() {
			return getRuleContexts(NumeroContext.class);
		}
		public NumeroContext numero(int i) {
			return getRuleContext(NumeroContext.class,i);
		}
		public TerminalNode LLD() { return getToken(FlowUmiParser.LLD, 0); }
		public BloqueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloque; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterBloque(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitBloque(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitBloque(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueContext bloque() throws RecognitionException {
		BloqueContext _localctx = new BloqueContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_bloque);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(582); match(LLI);
			setState(599);
			switch (_input.LA(1)) {
			case NOMBRE:
				{
				{
				setState(583); match(NOMBRE);
				setState(588);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(584); match(T__15);
					setState(585); match(NOMBRE);
					}
					}
					setState(590);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				break;
			case INTEGER:
			case FLOAT:
				{
				{
				setState(591); numero();
				setState(596);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(592); match(T__15);
					setState(593); numero();
					}
					}
					setState(598);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(601); match(LLD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloquepContext extends ParserRuleContext {
		public List<SentenciaContext> sentencia() {
			return getRuleContexts(SentenciaContext.class);
		}
		public TerminalNode LLI() { return getToken(FlowUmiParser.LLI, 0); }
		public OperacionContext operacion(int i) {
			return getRuleContext(OperacionContext.class,i);
		}
		public List<OperacionContext> operacion() {
			return getRuleContexts(OperacionContext.class);
		}
		public SentenciaContext sentencia(int i) {
			return getRuleContext(SentenciaContext.class,i);
		}
		public TerminalNode LLD() { return getToken(FlowUmiParser.LLD, 0); }
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public BloquepContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloquep; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterBloquep(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitBloquep(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitBloquep(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloquepContext bloquep() throws RecognitionException {
		BloquepContext _localctx = new BloquepContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_bloquep);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(603); match(LLI);
			setState(609);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__34) | (1L << T__30) | (1L << T__28) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__17) | (1L << T__16) | (1L << T__14) | (1L << T__13) | (1L << T__12) | (1L << T__11) | (1L << T__8) | (1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__0) | (1L << NOMBRE) | (1L << LLI) | (1L << CCI) | (1L << PLUS) | (1L << MINUS))) != 0)) {
				{
				setState(607);
				switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
				case 1:
					{
					setState(604); variable();
					}
					break;
				case 2:
					{
					setState(605); operacion();
					}
					break;
				case 3:
					{
					setState(606); sentencia();
					}
					break;
				}
				}
				setState(611);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(612); match(LLD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueswitchContext extends ParserRuleContext {
		public List<SentenciaContext> sentencia() {
			return getRuleContexts(SentenciaContext.class);
		}
		public OperacionContext operacion(int i) {
			return getRuleContext(OperacionContext.class,i);
		}
		public List<OperacionContext> operacion() {
			return getRuleContexts(OperacionContext.class);
		}
		public SentenciaContext sentencia(int i) {
			return getRuleContext(SentenciaContext.class,i);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public TerminalNode PUNTOYCOMA() { return getToken(FlowUmiParser.PUNTOYCOMA, 0); }
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public BloqueswitchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloqueswitch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterBloqueswitch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitBloqueswitch(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitBloqueswitch(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueswitchContext bloqueswitch() throws RecognitionException {
		BloqueswitchContext _localctx = new BloqueswitchContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_bloqueswitch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(619);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__34) | (1L << T__30) | (1L << T__28) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__17) | (1L << T__16) | (1L << T__14) | (1L << T__13) | (1L << T__12) | (1L << T__11) | (1L << T__8) | (1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__0) | (1L << NOMBRE) | (1L << LLI) | (1L << CCI) | (1L << PLUS) | (1L << MINUS))) != 0)) {
				{
				setState(617);
				switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
				case 1:
					{
					setState(614); variable();
					}
					break;
				case 2:
					{
					setState(615); operacion();
					}
					break;
				case 3:
					{
					setState(616); sentencia();
					}
					break;
				}
				}
				setState(621);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(622); match(T__23);
			setState(623); match(PUNTOYCOMA);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloquesentenciasContext extends ParserRuleContext {
		public List<SentenciaContext> sentencia() {
			return getRuleContexts(SentenciaContext.class);
		}
		public ValorContext valor() {
			return getRuleContext(ValorContext.class,0);
		}
		public TerminalNode LLI() { return getToken(FlowUmiParser.LLI, 0); }
		public OperacionContext operacion(int i) {
			return getRuleContext(OperacionContext.class,i);
		}
		public List<OperacionContext> operacion() {
			return getRuleContexts(OperacionContext.class);
		}
		public SentenciaContext sentencia(int i) {
			return getRuleContext(SentenciaContext.class,i);
		}
		public TerminalNode LLD() { return getToken(FlowUmiParser.LLD, 0); }
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public TerminalNode PUNTOYCOMA() { return getToken(FlowUmiParser.PUNTOYCOMA, 0); }
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public BloquesentenciasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloquesentencias; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterBloquesentencias(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitBloquesentencias(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitBloquesentencias(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloquesentenciasContext bloquesentencias() throws RecognitionException {
		BloquesentenciasContext _localctx = new BloquesentenciasContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_bloquesentencias);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(625); match(LLI);
			setState(631);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__34) | (1L << T__30) | (1L << T__28) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__17) | (1L << T__16) | (1L << T__14) | (1L << T__13) | (1L << T__12) | (1L << T__11) | (1L << T__8) | (1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__0) | (1L << NOMBRE) | (1L << LLI) | (1L << CCI) | (1L << PLUS) | (1L << MINUS))) != 0)) {
				{
				setState(629);
				switch ( getInterpreter().adaptivePredict(_input,45,_ctx) ) {
				case 1:
					{
					setState(626); variable();
					}
					break;
				case 2:
					{
					setState(627); operacion();
					}
					break;
				case 3:
					{
					setState(628); sentencia();
					}
					break;
				}
				}
				setState(633);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(634); match(T__33);
			setState(635); valor();
			setState(636); match(PUNTOYCOMA);
			setState(637); match(LLD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BloqueVoidContext extends ParserRuleContext {
		public List<SentenciaContext> sentencia() {
			return getRuleContexts(SentenciaContext.class);
		}
		public TerminalNode LLI() { return getToken(FlowUmiParser.LLI, 0); }
		public OperacionContext operacion(int i) {
			return getRuleContext(OperacionContext.class,i);
		}
		public List<OperacionContext> operacion() {
			return getRuleContexts(OperacionContext.class);
		}
		public SentenciaContext sentencia(int i) {
			return getRuleContext(SentenciaContext.class,i);
		}
		public TerminalNode LLD() { return getToken(FlowUmiParser.LLD, 0); }
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public BloqueVoidContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bloqueVoid; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterBloqueVoid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitBloqueVoid(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitBloqueVoid(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BloqueVoidContext bloqueVoid() throws RecognitionException {
		BloqueVoidContext _localctx = new BloqueVoidContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_bloqueVoid);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(639); match(LLI);
			setState(645);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__41) | (1L << T__40) | (1L << T__34) | (1L << T__30) | (1L << T__28) | (1L << T__27) | (1L << T__22) | (1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__18) | (1L << T__17) | (1L << T__16) | (1L << T__14) | (1L << T__13) | (1L << T__12) | (1L << T__11) | (1L << T__8) | (1L << T__7) | (1L << T__6) | (1L << T__5) | (1L << T__0) | (1L << NOMBRE) | (1L << LLI) | (1L << CCI) | (1L << PLUS) | (1L << MINUS))) != 0)) {
				{
				setState(643);
				switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
				case 1:
					{
					setState(640); variable();
					}
					break;
				case 2:
					{
					setState(641); operacion();
					}
					break;
				case 3:
					{
					setState(642); sentencia();
					}
					break;
				}
				}
				setState(647);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(648); match(LLD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixContext extends ParserRuleContext {
		public TerminalNode MINUS(int i) {
			return getToken(FlowUmiParser.MINUS, i);
		}
		public TerminalNode CCD() { return getToken(FlowUmiParser.CCD, 0); }
		public TerminalNode LLI() { return getToken(FlowUmiParser.LLI, 0); }
		public TerminalNode CCI() { return getToken(FlowUmiParser.CCI, 0); }
		public List<TerminalNode> PLUS() { return getTokens(FlowUmiParser.PLUS); }
		public List<TerminalNode> MINUS() { return getTokens(FlowUmiParser.MINUS); }
		public TerminalNode LLD() { return getToken(FlowUmiParser.LLD, 0); }
		public TerminalNode PLUS(int i) {
			return getToken(FlowUmiParser.PLUS, i);
		}
		public PostfixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterPostfix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitPostfix(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitPostfix(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostfixContext postfix() throws RecognitionException {
		PostfixContext _localctx = new PostfixContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_postfix);
		try {
			setState(659);
			switch (_input.LA(1)) {
			case LLI:
				enterOuterAlt(_localctx, 1);
				{
				setState(650); match(LLI);
				setState(651); match(LLD);
				}
				break;
			case CCI:
				enterOuterAlt(_localctx, 2);
				{
				setState(652); match(CCI);
				setState(653); match(CCD);
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 3);
				{
				setState(654); match(T__14);
				}
				break;
			case PLUS:
				enterOuterAlt(_localctx, 4);
				{
				setState(655); match(PLUS);
				setState(656); match(PLUS);
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 5);
				{
				setState(657); match(MINUS);
				setState(658); match(MINUS);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public TerminalNode MULT() { return getToken(FlowUmiParser.MULT, 0); }
		public TerminalNode PLUS() { return getToken(FlowUmiParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(FlowUmiParser.MINUS, 0); }
		public TerminalNode MOD() { return getToken(FlowUmiParser.MOD, 0); }
		public TerminalNode IGUAL() { return getToken(FlowUmiParser.IGUAL, 0); }
		public TerminalNode DIV() { return getToken(FlowUmiParser.DIV, 0); }
		public TerminalNode POW() { return getToken(FlowUmiParser.POW, 0); }
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitAssignment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitAssignment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_assignment);
		try {
			setState(673);
			switch (_input.LA(1)) {
			case PLUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(661); match(PLUS);
				setState(662); match(IGUAL);
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 2);
				{
				setState(663); match(MINUS);
				setState(664); match(IGUAL);
				}
				break;
			case MULT:
				enterOuterAlt(_localctx, 3);
				{
				setState(665); match(MULT);
				setState(666); match(IGUAL);
				}
				break;
			case DIV:
				enterOuterAlt(_localctx, 4);
				{
				setState(667); match(DIV);
				setState(668); match(IGUAL);
				}
				break;
			case MOD:
				enterOuterAlt(_localctx, 5);
				{
				setState(669); match(MOD);
				setState(670); match(IGUAL);
				}
				break;
			case POW:
				enterOuterAlt(_localctx, 6);
				{
				setState(671); match(POW);
				setState(672); match(IGUAL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForincrementContext extends ParserRuleContext {
		public TerminalNode MINUS(int i) {
			return getToken(FlowUmiParser.MINUS, i);
		}
		public List<TerminalNode> PLUS() { return getTokens(FlowUmiParser.PLUS); }
		public List<TerminalNode> MINUS() { return getTokens(FlowUmiParser.MINUS); }
		public TerminalNode PLUS(int i) {
			return getToken(FlowUmiParser.PLUS, i);
		}
		public ForincrementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forincrement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).enterForincrement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof FlowUmiListener ) ((FlowUmiListener)listener).exitForincrement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof FlowUmiVisitor ) return ((FlowUmiVisitor<? extends T>)visitor).visitForincrement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForincrementContext forincrement() throws RecognitionException {
		ForincrementContext _localctx = new ForincrementContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_forincrement);
		try {
			setState(679);
			switch (_input.LA(1)) {
			case PLUS:
				enterOuterAlt(_localctx, 1);
				{
				setState(675); match(PLUS);
				setState(676); match(PLUS);
				}
				break;
			case MINUS:
				enterOuterAlt(_localctx, 2);
				{
				setState(677); match(MINUS);
				setState(678); match(MINUS);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3H\u02ac\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\3\2\3\2\7\2c\n\2\f\2\16\2f\13\2\3\2\3"+
		"\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\5\5s\n\5\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6\u0084\n\6\f\6\16\6\u0087\13"+
		"\6\7\6\u0089\n\6\f\6\16\6\u008c\13\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7"+
		"\6\u0096\n\6\f\6\16\6\u0099\13\6\7\6\u009b\n\6\f\6\16\6\u009e\13\6\3\6"+
		"\3\6\5\6\u00a2\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00ae\n"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t"+
		"\3\t\3\t\7\t\u00c3\n\t\f\t\16\t\u00c6\13\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\7\n\u00d1\n\n\f\n\16\n\u00d4\13\n\3\13\3\13\3\13\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\6\r\u00e4\n\r\r\r\16\r\u00e5\3\r\3\r"+
		"\3\r\7\r\u00eb\n\r\f\r\16\r\u00ee\13\r\3\r\3\r\3\16\3\16\3\16\5\16\u00f5"+
		"\n\16\3\17\3\17\3\17\3\17\3\17\5\17\u00fc\n\17\7\17\u00fe\n\17\f\17\16"+
		"\17\u0101\13\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\7\21\u010b\n\21"+
		"\f\21\16\21\u010e\13\21\3\22\3\22\3\22\7\22\u0113\n\22\f\22\16\22\u0116"+
		"\13\22\3\23\3\23\3\23\7\23\u011b\n\23\f\23\16\23\u011e\13\23\3\24\3\24"+
		"\3\24\7\24\u0123\n\24\f\24\16\24\u0126\13\24\3\25\3\25\3\25\7\25\u012b"+
		"\n\25\f\25\16\25\u012e\13\25\3\26\3\26\3\26\7\26\u0133\n\26\f\26\16\26"+
		"\u0136\13\26\3\27\3\27\3\27\7\27\u013b\n\27\f\27\16\27\u013e\13\27\3\30"+
		"\3\30\3\30\3\30\3\30\5\30\u0145\n\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33"+
		"\3\33\3\33\3\33\3\33\3\33\3\33\5\33\u0154\n\33\3\33\3\33\3\33\3\33\5\33"+
		"\u015a\n\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33"+
		"\3\33\5\33\u0169\n\33\3\33\3\33\5\33\u016d\n\33\3\34\3\34\3\34\3\34\3"+
		"\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3"+
		"\34\3\34\5\34\u0183\n\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u0194\n\34\f\34\16\34\u0197\13\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u019f\n\34\f\34\16\34\u01a2\13\34"+
		"\3\34\3\34\3\34\3\34\7\34\u01a8\n\34\f\34\16\34\u01ab\13\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\7\34\u01b3\n\34\f\34\16\34\u01b6\13\34\3\34\3\34"+
		"\3\34\3\34\7\34\u01bc\n\34\f\34\16\34\u01bf\13\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u01ec\n\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u01f9\n\34"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\5\35\u020d\n\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u022d\n\35\3\36\3\36"+
		"\3\37\3\37\3 \3 \3 \3 \5 \u0237\n \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&"+
		"\3&\3\'\3\'\3(\3(\3)\3)\3)\3)\7)\u024d\n)\f)\16)\u0250\13)\3)\3)\3)\7"+
		")\u0255\n)\f)\16)\u0258\13)\5)\u025a\n)\3)\3)\3*\3*\3*\3*\7*\u0262\n*"+
		"\f*\16*\u0265\13*\3*\3*\3+\3+\3+\7+\u026c\n+\f+\16+\u026f\13+\3+\3+\3"+
		"+\3,\3,\3,\3,\7,\u0278\n,\f,\16,\u027b\13,\3,\3,\3,\3,\3,\3-\3-\3-\3-"+
		"\7-\u0286\n-\f-\16-\u0289\13-\3-\3-\3.\3.\3.\3.\3.\3.\3.\3.\3.\5.\u0296"+
		"\n.\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\5/\u02a4\n/\3\60\3\60\3\60\3\60"+
		"\5\60\u02aa\n\60\3\60\2\2\61\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \""+
		"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^\2\16\4\2\6\6##\4\2\r\r++\3\2-"+
		".\3\2AB\3\2CF\3\2;<\3\2=?\4\2-.\60\60\7\2\21\21\26\26\30\32\37 &&\5\2"+
		"\4\4\16\16\27\27\b\2\7\7\t\t\17\17\24\24\"\"((\4\2..\60\60\u02dd\2`\3"+
		"\2\2\2\4j\3\2\2\2\6l\3\2\2\2\br\3\2\2\2\n\u00a1\3\2\2\2\f\u00a3\3\2\2"+
		"\2\16\u00b7\3\2\2\2\20\u00bf\3\2\2\2\22\u00ca\3\2\2\2\24\u00d5\3\2\2\2"+
		"\26\u00d8\3\2\2\2\30\u00de\3\2\2\2\32\u00f4\3\2\2\2\34\u00f6\3\2\2\2\36"+
		"\u0105\3\2\2\2 \u0107\3\2\2\2\"\u010f\3\2\2\2$\u0117\3\2\2\2&\u011f\3"+
		"\2\2\2(\u0127\3\2\2\2*\u012f\3\2\2\2,\u0137\3\2\2\2.\u0144\3\2\2\2\60"+
		"\u0146\3\2\2\2\62\u0148\3\2\2\2\64\u016c\3\2\2\2\66\u01f8\3\2\2\28\u022c"+
		"\3\2\2\2:\u022e\3\2\2\2<\u0230\3\2\2\2>\u0236\3\2\2\2@\u0238\3\2\2\2B"+
		"\u023a\3\2\2\2D\u023c\3\2\2\2F\u023e\3\2\2\2H\u0240\3\2\2\2J\u0242\3\2"+
		"\2\2L\u0244\3\2\2\2N\u0246\3\2\2\2P\u0248\3\2\2\2R\u025d\3\2\2\2T\u026d"+
		"\3\2\2\2V\u0273\3\2\2\2X\u0281\3\2\2\2Z\u0295\3\2\2\2\\\u02a3\3\2\2\2"+
		"^\u02a9\3\2\2\2`d\5\4\3\2ac\5\b\5\2ba\3\2\2\2cf\3\2\2\2db\3\2\2\2de\3"+
		"\2\2\2eg\3\2\2\2fd\3\2\2\2gh\5\6\4\2hi\7\2\2\3i\3\3\2\2\2jk\t\2\2\2k\5"+
		"\3\2\2\2lm\t\3\2\2m\7\3\2\2\2ns\5\66\34\2os\5\64\33\2ps\5\n\6\2qs\5\32"+
		"\16\2rn\3\2\2\2ro\3\2\2\2rp\3\2\2\2rq\3\2\2\2s\t\3\2\2\2t\u00a2\5\22\n"+
		"\2u\u00a2\5\20\t\2v\u00a2\5\26\f\2w\u00a2\5\16\b\2x\u00a2\5\f\7\2yz\5"+
		":\36\2z{\7-\2\2{|\5P)\2|}\7\61\2\2}\u00a2\3\2\2\2~\177\5:\36\2\177\u0080"+
		"\7-\2\2\u0080\u008a\78\2\2\u0081\u0085\58\35\2\u0082\u0084\7\35\2\2\u0083"+
		"\u0082\3\2\2\2\u0084\u0087\3\2\2\2\u0085\u0083\3\2\2\2\u0085\u0086\3\2"+
		"\2\2\u0086\u0089\3\2\2\2\u0087\u0085\3\2\2\2\u0088\u0081\3\2\2\2\u0089"+
		"\u008c\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008d\3\2"+
		"\2\2\u008c\u008a\3\2\2\2\u008d\u008e\79\2\2\u008e\u008f\5V,\2\u008f\u00a2"+
		"\3\2\2\2\u0090\u0091\7\20\2\2\u0091\u0092\7-\2\2\u0092\u009c\78\2\2\u0093"+
		"\u0097\58\35\2\u0094\u0096\7\35\2\2\u0095\u0094\3\2\2\2\u0096\u0099\3"+
		"\2\2\2\u0097\u0095\3\2\2\2\u0097\u0098\3\2\2\2\u0098\u009b\3\2\2\2\u0099"+
		"\u0097\3\2\2\2\u009a\u0093\3\2\2\2\u009b\u009e\3\2\2\2\u009c\u009a\3\2"+
		"\2\2\u009c\u009d\3\2\2\2\u009d\u009f\3\2\2\2\u009e\u009c\3\2\2\2\u009f"+
		"\u00a0\79\2\2\u00a0\u00a2\5X-\2\u00a1t\3\2\2\2\u00a1u\3\2\2\2\u00a1v\3"+
		"\2\2\2\u00a1w\3\2\2\2\u00a1x\3\2\2\2\u00a1y\3\2\2\2\u00a1~\3\2\2\2\u00a1"+
		"\u0090\3\2\2\2\u00a2\13\3\2\2\2\u00a3\u00a4\7\34\2\2\u00a4\u00ad\78\2"+
		"\2\u00a5\u00a6\7\31\2\2\u00a6\u00a7\7-\2\2\u00a7\u00a8\7\63\2\2\u00a8"+
		"\u00ae\t\4\2\2\u00a9\u00ae\7-\2\2\u00aa\u00ab\7-\2\2\u00ab\u00ac\7\63"+
		"\2\2\u00ac\u00ae\t\4\2\2\u00ad\u00a5\3\2\2\2\u00ad\u00a9\3\2\2\2\u00ad"+
		"\u00aa\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b0\7\61\2\2\u00b0\u00b1\5"+
		"\36\20\2\u00b1\u00b2\7\61\2\2\u00b2\u00b3\7-\2\2\u00b3\u00b4\5^\60\2\u00b4"+
		"\u00b5\79\2\2\u00b5\u00b6\5R*\2\u00b6\r\3\2\2\2\u00b7\u00b8\7\33\2\2\u00b8"+
		"\u00b9\5R*\2\u00b9\u00ba\7$\2\2\u00ba\u00bb\78\2\2\u00bb\u00bc\5\36\20"+
		"\2\u00bc\u00bd\79\2\2\u00bd\u00be\7\61\2\2\u00be\17\3\2\2\2\u00bf\u00c0"+
		"\7$\2\2\u00c0\u00c4\78\2\2\u00c1\u00c3\5\36\20\2\u00c2\u00c1\3\2\2\2\u00c3"+
		"\u00c6\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c4\u00c5\3\2\2\2\u00c5\u00c7\3\2"+
		"\2\2\u00c6\u00c4\3\2\2\2\u00c7\u00c8\79\2\2\u00c8\u00c9\5R*\2\u00c9\21"+
		"\3\2\2\2\u00ca\u00cb\7%\2\2\u00cb\u00cc\78\2\2\u00cc\u00cd\5\32\16\2\u00cd"+
		"\u00ce\79\2\2\u00ce\u00d2\5R*\2\u00cf\u00d1\5\24\13\2\u00d0\u00cf\3\2"+
		"\2\2\u00d1\u00d4\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3"+
		"\23\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d5\u00d6\7\b\2\2\u00d6\u00d7\5R*\2"+
		"\u00d7\25\3\2\2\2\u00d8\u00d9\7,\2\2\u00d9\u00da\78\2\2\u00da\u00db\5"+
		"\32\16\2\u00db\u00dc\79\2\2\u00dc\u00dd\5\30\r\2\u00dd\27\3\2\2\2\u00de"+
		"\u00e3\7\64\2\2\u00df\u00e0\7)\2\2\u00e0\u00e1\7-\2\2\u00e1\u00e2\7\62"+
		"\2\2\u00e2\u00e4\5T+\2\u00e3\u00df\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5\u00e3"+
		"\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00ec\3\2\2\2\u00e7\u00e8\7\5\2\2\u00e8"+
		"\u00e9\7\62\2\2\u00e9\u00eb\5T+\2\u00ea\u00e7\3\2\2\2\u00eb\u00ee\3\2"+
		"\2\2\u00ec\u00ea\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed\u00ef\3\2\2\2\u00ee"+
		"\u00ec\3\2\2\2\u00ef\u00f0\7\65\2\2\u00f0\31\3\2\2\2\u00f1\u00f5\5\36"+
		"\20\2\u00f2\u00f5\5(\25\2\u00f3\u00f5\5\34\17\2\u00f4\u00f1\3\2\2\2\u00f4"+
		"\u00f2\3\2\2\2\u00f4\u00f3\3\2\2\2\u00f5\33\3\2\2\2\u00f6\u00f7\7-\2\2"+
		"\u00f7\u00ff\78\2\2\u00f8\u00fb\5:\36\2\u00f9\u00fc\7-\2\2\u00fa\u00fc"+
		"\5\62\32\2\u00fb\u00f9\3\2\2\2\u00fb\u00fa\3\2\2\2\u00fc\u00fe\3\2\2\2"+
		"\u00fd\u00f8\3\2\2\2\u00fe\u0101\3\2\2\2\u00ff\u00fd\3\2\2\2\u00ff\u0100"+
		"\3\2\2\2\u0100\u0102\3\2\2\2\u0101\u00ff\3\2\2\2\u0102\u0103\79\2\2\u0103"+
		"\u0104\7\61\2\2\u0104\35\3\2\2\2\u0105\u0106\5 \21\2\u0106\37\3\2\2\2"+
		"\u0107\u010c\5\"\22\2\u0108\u0109\7G\2\2\u0109\u010b\5\"\22\2\u010a\u0108"+
		"\3\2\2\2\u010b\u010e\3\2\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d"+
		"!\3\2\2\2\u010e\u010c\3\2\2\2\u010f\u0114\5$\23\2\u0110\u0111\t\5\2\2"+
		"\u0111\u0113\5$\23\2\u0112\u0110\3\2\2\2\u0113\u0116\3\2\2\2\u0114\u0112"+
		"\3\2\2\2\u0114\u0115\3\2\2\2\u0115#\3\2\2\2\u0116\u0114\3\2\2\2\u0117"+
		"\u011c\5&\24\2\u0118\u0119\7H\2\2\u0119\u011b\5&\24\2\u011a\u0118\3\2"+
		"\2\2\u011b\u011e\3\2\2\2\u011c\u011a\3\2\2\2\u011c\u011d\3\2\2\2\u011d"+
		"%\3\2\2\2\u011e\u011c\3\2\2\2\u011f\u0124\5(\25\2\u0120\u0121\t\6\2\2"+
		"\u0121\u0123\5(\25\2\u0122\u0120\3\2\2\2\u0123\u0126\3\2\2\2\u0124\u0122"+
		"\3\2\2\2\u0124\u0125\3\2\2\2\u0125\'\3\2\2\2\u0126\u0124\3\2\2\2\u0127"+
		"\u012c\5*\26\2\u0128\u0129\t\7\2\2\u0129\u012b\5*\26\2\u012a\u0128\3\2"+
		"\2\2\u012b\u012e\3\2\2\2\u012c\u012a\3\2\2\2\u012c\u012d\3\2\2\2\u012d"+
		")\3\2\2\2\u012e\u012c\3\2\2\2\u012f\u0134\5,\27\2\u0130\u0131\t\b\2\2"+
		"\u0131\u0133\5,\27\2\u0132\u0130\3\2\2\2\u0133\u0136\3\2\2\2\u0134\u0132"+
		"\3\2\2\2\u0134\u0135\3\2\2\2\u0135+\3\2\2\2\u0136\u0134\3\2\2\2\u0137"+
		"\u013c\5.\30\2\u0138\u0139\7@\2\2\u0139\u013b\5.\30\2\u013a\u0138\3\2"+
		"\2\2\u013b\u013e\3\2\2\2\u013c\u013a\3\2\2\2\u013c\u013d\3\2\2\2\u013d"+
		"-\3\2\2\2\u013e\u013c\3\2\2\2\u013f\u0145\5\60\31\2\u0140\u0141\7:\2\2"+
		"\u0141\u0145\5\60\31\2\u0142\u0143\7<\2\2\u0143\u0145\5.\30\2\u0144\u013f"+
		"\3\2\2\2\u0144\u0140\3\2\2\2\u0144\u0142\3\2\2\2\u0145/\3\2\2\2\u0146"+
		"\u0147\5\62\32\2\u0147\61\3\2\2\2\u0148\u0149\t\t\2\2\u0149\63\3\2\2\2"+
		"\u014a\u014b\7-\2\2\u014b\u014c\7\63\2\2\u014c\u014d\5\32\16\2\u014d\u014e"+
		"\7\61\2\2\u014e\u016d\3\2\2\2\u014f\u0150\7-\2\2\u0150\u0153\7\66\2\2"+
		"\u0151\u0154\5N(\2\u0152\u0154\7-\2\2\u0153\u0151\3\2\2\2\u0153\u0152"+
		"\3\2\2\2\u0154\u0155\3\2\2\2\u0155\u0156\7\67\2\2\u0156\u0159\7\63\2\2"+
		"\u0157\u015a\5N(\2\u0158\u015a\7-\2\2\u0159\u0157\3\2\2\2\u0159\u0158"+
		"\3\2\2\2\u015a\u015b\3\2\2\2\u015b\u016d\7\61\2\2\u015c\u015d\5Z.\2\u015d"+
		"\u015e\7-\2\2\u015e\u015f\7\61\2\2\u015f\u016d\3\2\2\2\u0160\u0161\7-"+
		"\2\2\u0161\u0162\5Z.\2\u0162\u0163\7\61\2\2\u0163\u016d\3\2\2\2\u0164"+
		"\u0165\7-\2\2\u0165\u0168\5\\/\2\u0166\u0169\7-\2\2\u0167\u0169\5N(\2"+
		"\u0168\u0166\3\2\2\2\u0168\u0167\3\2\2\2\u0169\u016a\3\2\2\2\u016a\u016b"+
		"\7\61\2\2\u016b\u016d\3\2\2\2\u016c\u014a\3\2\2\2\u016c\u014f\3\2\2\2"+
		"\u016c\u015c\3\2\2\2\u016c\u0160\3\2\2\2\u016c\u0164\3\2\2\2\u016d\65"+
		"\3\2\2\2\u016e\u016f\5L\'\2\u016f\u0170\5:\36\2\u0170\u0171\7-\2\2\u0171"+
		"\u0172\7\61\2\2\u0172\u01f9\3\2\2\2\u0173\u0174\5L\'\2\u0174\u0175\5>"+
		" \2\u0175\u0176\7-\2\2\u0176\u0177\7\63\2\2\u0177\u0178\5N(\2\u0178\u0179"+
		"\7\61\2\2\u0179\u01f9\3\2\2\2\u017a\u017b\5L\'\2\u017b\u017c\5J&\2\u017c"+
		"\u017d\7-\2\2\u017d\u0182\7\63\2\2\u017e\u0183\7-\2\2\u017f\u0180\7*\2"+
		"\2\u0180\u0181\7-\2\2\u0181\u0183\7*\2\2\u0182\u017e\3\2\2\2\u0182\u017f"+
		"\3\2\2\2\u0183\u0184\3\2\2\2\u0184\u0185\7\61\2\2\u0185\u01f9\3\2\2\2"+
		"\u0186\u0187\5:\36\2\u0187\u0188\7-\2\2\u0188\u0189\5P)\2\u0189\u018a"+
		"\7\61\2\2\u018a\u01f9\3\2\2\2\u018b\u018c\5:\36\2\u018c\u018d\7-\2\2\u018d"+
		"\u018e\7\61\2\2\u018e\u01f9\3\2\2\2\u018f\u0190\5:\36\2\u0190\u0195\7"+
		"-\2\2\u0191\u0192\7\35\2\2\u0192\u0194\7-\2\2\u0193\u0191\3\2\2\2\u0194"+
		"\u0197\3\2\2\2\u0195\u0193\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u0198\3\2"+
		"\2\2\u0197\u0195\3\2\2\2\u0198\u0199\7\61\2\2\u0199\u01f9\3\2\2\2\u019a"+
		"\u019b\5:\36\2\u019b\u01a0\7-\2\2\u019c\u019d\7\35\2\2\u019d\u019f\7-"+
		"\2\2\u019e\u019c\3\2\2\2\u019f\u01a2\3\2\2\2\u01a0\u019e\3\2\2\2\u01a0"+
		"\u01a1\3\2\2\2\u01a1\u01a3\3\2\2\2\u01a2\u01a0\3\2\2\2\u01a3\u01a4\7\63"+
		"\2\2\u01a4\u01a9\7-\2\2\u01a5\u01a6\7\35\2\2\u01a6\u01a8\7-\2\2\u01a7"+
		"\u01a5\3\2\2\2\u01a8\u01ab\3\2\2\2\u01a9\u01a7\3\2\2\2\u01a9\u01aa\3\2"+
		"\2\2\u01aa\u01ac\3\2\2\2\u01ab\u01a9\3\2\2\2\u01ac\u01ad\7\61\2\2\u01ad"+
		"\u01f9\3\2\2\2\u01ae\u01af\5:\36\2\u01af\u01b4\7-\2\2\u01b0\u01b1\7\35"+
		"\2\2\u01b1\u01b3\7-\2\2\u01b2\u01b0\3\2\2\2\u01b3\u01b6\3\2\2\2\u01b4"+
		"\u01b2\3\2\2\2\u01b4\u01b5\3\2\2\2\u01b5\u01b7\3\2\2\2\u01b6\u01b4\3\2"+
		"\2\2\u01b7\u01b8\7\63\2\2\u01b8\u01bd\5N(\2\u01b9\u01ba\7\35\2\2\u01ba"+
		"\u01bc\5N(\2\u01bb\u01b9\3\2\2\2\u01bc\u01bf\3\2\2\2\u01bd\u01bb\3\2\2"+
		"\2\u01bd\u01be\3\2\2\2\u01be\u01c0\3\2\2\2\u01bf\u01bd\3\2\2\2\u01c0\u01c1"+
		"\7\61\2\2\u01c1\u01f9\3\2\2\2\u01c2\u01c3\5:\36\2\u01c3\u01c4\7-\2\2\u01c4"+
		"\u01c5\7\63\2\2\u01c5\u01c6\7-\2\2\u01c6\u01c7\7\61\2\2\u01c7\u01f9\3"+
		"\2\2\2\u01c8\u01c9\5:\36\2\u01c9\u01ca\7-\2\2\u01ca\u01cb\7\63\2\2\u01cb"+
		"\u01cc\5N(\2\u01cc\u01cd\7\61\2\2\u01cd\u01f9\3\2\2\2\u01ce\u01cf\7\n"+
		"\2\2\u01cf\u01d0\7-\2\2\u01d0\u01d1\7\63\2\2\u01d1\u01d2\7\23\2\2\u01d2"+
		"\u01d3\7/\2\2\u01d3\u01d4\7\23\2\2\u01d4\u01f9\7\61\2\2\u01d5\u01d6\7"+
		"\n\2\2\u01d6\u01d7\7\66\2\2\u01d7\u01d8\7\67\2\2\u01d8\u01d9\7-\2\2\u01d9"+
		"\u01da\7\63\2\2\u01da\u01db\7\f\2\2\u01db\u01dc\7\n\2\2\u01dc\u01dd\7"+
		"\66\2\2\u01dd\u01de\5N(\2\u01de\u01df\7\67\2\2\u01df\u01e0\7\61\2\2\u01e0"+
		"\u01f9\3\2\2\2\u01e1\u01e2\5:\36\2\u01e2\u01e3\7\66\2\2\u01e3\u01e4\7"+
		"\67\2\2\u01e4\u01e5\7-\2\2\u01e5\u01e6\7\63\2\2\u01e6\u01e7\7\f\2\2\u01e7"+
		"\u01e8\5:\36\2\u01e8\u01eb\7\66\2\2\u01e9\u01ec\5N(\2\u01ea\u01ec\7-\2"+
		"\2\u01eb\u01e9\3\2\2\2\u01eb\u01ea\3\2\2\2\u01ec\u01ed\3\2\2\2\u01ed\u01ee"+
		"\7\67\2\2\u01ee\u01ef\7\61\2\2\u01ef\u01f9\3\2\2\2\u01f0\u01f1\5:\36\2"+
		"\u01f1\u01f2\7\66\2\2\u01f2\u01f3\7\67\2\2\u01f3\u01f4\7-\2\2\u01f4\u01f5"+
		"\7\63\2\2\u01f5\u01f6\5P)\2\u01f6\u01f7\7\61\2\2\u01f7\u01f9\3\2\2\2\u01f8"+
		"\u016e\3\2\2\2\u01f8\u0173\3\2\2\2\u01f8\u017a\3\2\2\2\u01f8\u0186\3\2"+
		"\2\2\u01f8\u018b\3\2\2\2\u01f8\u018f\3\2\2\2\u01f8\u019a\3\2\2\2\u01f8"+
		"\u01ae\3\2\2\2\u01f8\u01c2\3\2\2\2\u01f8\u01c8\3\2\2\2\u01f8\u01ce\3\2"+
		"\2\2\u01f8\u01d5\3\2\2\2\u01f8\u01e1\3\2\2\2\u01f8\u01f0\3\2\2\2\u01f9"+
		"\67\3\2\2\2\u01fa\u01fb\5L\'\2\u01fb\u01fc\5:\36\2\u01fc\u01fd\7-\2\2"+
		"\u01fd\u022d\3\2\2\2\u01fe\u01ff\5L\'\2\u01ff\u0200\5> \2\u0200\u0201"+
		"\7-\2\2\u0201\u0202\7\63\2\2\u0202\u0203\5N(\2\u0203\u022d\3\2\2\2\u0204"+
		"\u0205\5L\'\2\u0205\u0206\5J&\2\u0206\u0207\7-\2\2\u0207\u020c\7\63\2"+
		"\2\u0208\u020d\7-\2\2\u0209\u020a\7*\2\2\u020a\u020b\7-\2\2\u020b\u020d"+
		"\7*\2\2\u020c\u0208\3\2\2\2\u020c\u0209\3\2\2\2\u020d\u022d\3\2\2\2\u020e"+
		"\u020f\5:\36\2\u020f\u0210\7-\2\2\u0210\u022d\3\2\2\2\u0211\u0212\5:\36"+
		"\2\u0212\u0213\7-\2\2\u0213\u0214\7\63\2\2\u0214\u0215\7-\2\2\u0215\u022d"+
		"\3\2\2\2\u0216\u0217\5:\36\2\u0217\u0218\7-\2\2\u0218\u0219\7\63\2\2\u0219"+
		"\u021a\5N(\2\u021a\u022d\3\2\2\2\u021b\u021c\7\n\2\2\u021c\u021d\7-\2"+
		"\2\u021d\u021e\7\63\2\2\u021e\u021f\7\23\2\2\u021f\u0220\7/\2\2\u0220"+
		"\u022d\7\23\2\2\u0221\u0222\7\n\2\2\u0222\u0223\7\66\2\2\u0223\u0224\7"+
		"\67\2\2\u0224\u0225\7-\2\2\u0225\u0226\7\63\2\2\u0226\u0227\7\f\2\2\u0227"+
		"\u0228\7\n\2\2\u0228\u0229\7\66\2\2\u0229\u022a\5N(\2\u022a\u022b\7\67"+
		"\2\2\u022b\u022d\3\2\2\2\u022c\u01fa\3\2\2\2\u022c\u01fe\3\2\2\2\u022c"+
		"\u0204\3\2\2\2\u022c\u020e\3\2\2\2\u022c\u0211\3\2\2\2\u022c\u0216\3\2"+
		"\2\2\u022c\u021b\3\2\2\2\u022c\u0221\3\2\2\2\u022d9\3\2\2\2\u022e\u022f"+
		"\5<\37\2\u022f;\3\2\2\2\u0230\u0231\5> \2\u0231=\3\2\2\2\u0232\u0237\5"+
		"@!\2\u0233\u0237\5B\"\2\u0234\u0237\5D#\2\u0235\u0237\5F$\2\u0236\u0232"+
		"\3\2\2\2\u0236\u0233\3\2\2\2\u0236\u0234\3\2\2\2\u0236\u0235\3\2\2\2\u0237"+
		"?\3\2\2\2\u0238\u0239\t\n\2\2\u0239A\3\2\2\2\u023a\u023b\t\13\2\2\u023b"+
		"C\3\2\2\2\u023c\u023d\7\3\2\2\u023dE\3\2\2\2\u023e\u023f\7\'\2\2\u023f"+
		"G\3\2\2\2\u0240\u0241\7\22\2\2\u0241I\3\2\2\2\u0242\u0243\t\f\2\2\u0243"+
		"K\3\2\2\2\u0244\u0245\7!\2\2\u0245M\3\2\2\2\u0246\u0247\t\r\2\2\u0247"+
		"O\3\2\2\2\u0248\u0259\7\64\2\2\u0249\u024e\7-\2\2\u024a\u024b\7\35\2\2"+
		"\u024b\u024d\7-\2\2\u024c\u024a\3\2\2\2\u024d\u0250\3\2\2\2\u024e\u024c"+
		"\3\2\2\2\u024e\u024f\3\2\2\2\u024f\u025a\3\2\2\2\u0250\u024e\3\2\2\2\u0251"+
		"\u0256\5N(\2\u0252\u0253\7\35\2\2\u0253\u0255\5N(\2\u0254\u0252\3\2\2"+
		"\2\u0255\u0258\3\2\2\2\u0256\u0254\3\2\2\2\u0256\u0257\3\2\2\2\u0257\u025a"+
		"\3\2\2\2\u0258\u0256\3\2\2\2\u0259\u0249\3\2\2\2\u0259\u0251\3\2\2\2\u025a"+
		"\u025b\3\2\2\2\u025b\u025c\7\65\2\2\u025cQ\3\2\2\2\u025d\u0263\7\64\2"+
		"\2\u025e\u0262\5\66\34\2\u025f\u0262\5\64\33\2\u0260\u0262\5\n\6\2\u0261"+
		"\u025e\3\2\2\2\u0261\u025f\3\2\2\2\u0261\u0260\3\2\2\2\u0262\u0265\3\2"+
		"\2\2\u0263\u0261\3\2\2\2\u0263\u0264\3\2\2\2\u0264\u0266\3\2\2\2\u0265"+
		"\u0263\3\2\2\2\u0266\u0267\7\65\2\2\u0267S\3\2\2\2\u0268\u026c\5\66\34"+
		"\2\u0269\u026c\5\64\33\2\u026a\u026c\5\n\6\2\u026b\u0268\3\2\2\2\u026b"+
		"\u0269\3\2\2\2\u026b\u026a\3\2\2\2\u026c\u026f\3\2\2\2\u026d\u026b\3\2"+
		"\2\2\u026d\u026e\3\2\2\2\u026e\u0270\3\2\2\2\u026f\u026d\3\2\2\2\u0270"+
		"\u0271\7\25\2\2\u0271\u0272\7\61\2\2\u0272U\3\2\2\2\u0273\u0279\7\64\2"+
		"\2\u0274\u0278\5\66\34\2\u0275\u0278\5\64\33\2\u0276\u0278\5\n\6\2\u0277"+
		"\u0274\3\2\2\2\u0277\u0275\3\2\2\2\u0277\u0276\3\2\2\2\u0278\u027b\3\2"+
		"\2\2\u0279\u0277\3\2\2\2\u0279\u027a\3\2\2\2\u027a\u027c\3\2\2\2\u027b"+
		"\u0279\3\2\2\2\u027c\u027d\7\13\2\2\u027d\u027e\5\62\32\2\u027e\u027f"+
		"\7\61\2\2\u027f\u0280\7\65\2\2\u0280W\3\2\2\2\u0281\u0287\7\64\2\2\u0282"+
		"\u0286\5\66\34\2\u0283\u0286\5\64\33\2\u0284\u0286\5\n\6\2\u0285\u0282"+
		"\3\2\2\2\u0285\u0283\3\2\2\2\u0285\u0284\3\2\2\2\u0286\u0289\3\2\2\2\u0287"+
		"\u0285\3\2\2\2\u0287\u0288\3\2\2\2\u0288\u028a\3\2\2\2\u0289\u0287\3\2"+
		"\2\2\u028a\u028b\7\65\2\2\u028bY\3\2\2\2\u028c\u028d\7\64\2\2\u028d\u0296"+
		"\7\65\2\2\u028e\u028f\7\66\2\2\u028f\u0296\7\67\2\2\u0290\u0296\7\36\2"+
		"\2\u0291\u0292\7;\2\2\u0292\u0296\7;\2\2\u0293\u0294\7<\2\2\u0294\u0296"+
		"\7<\2\2\u0295\u028c\3\2\2\2\u0295\u028e\3\2\2\2\u0295\u0290\3\2\2\2\u0295"+
		"\u0291\3\2\2\2\u0295\u0293\3\2\2\2\u0296[\3\2\2\2\u0297\u0298\7;\2\2\u0298"+
		"\u02a4\7\63\2\2\u0299\u029a\7<\2\2\u029a\u02a4\7\63\2\2\u029b\u029c\7"+
		"=\2\2\u029c\u02a4\7\63\2\2\u029d\u029e\7>\2\2\u029e\u02a4\7\63\2\2\u029f"+
		"\u02a0\7?\2\2\u02a0\u02a4\7\63\2\2\u02a1\u02a2\7@\2\2\u02a2\u02a4\7\63"+
		"\2\2\u02a3\u0297\3\2\2\2\u02a3\u0299\3\2\2\2\u02a3\u029b\3\2\2\2\u02a3"+
		"\u029d\3\2\2\2\u02a3\u029f\3\2\2\2\u02a3\u02a1\3\2\2\2\u02a4]\3\2\2\2"+
		"\u02a5\u02a6\7;\2\2\u02a6\u02aa\7;\2\2\u02a7\u02a8\7<\2\2\u02a8\u02aa"+
		"\7<\2\2\u02a9\u02a5\3\2\2\2\u02a9\u02a7\3\2\2\2\u02aa_\3\2\2\2\66dr\u0085"+
		"\u008a\u0097\u009c\u00a1\u00ad\u00c4\u00d2\u00e5\u00ec\u00f4\u00fb\u00ff"+
		"\u010c\u0114\u011c\u0124\u012c\u0134\u013c\u0144\u0153\u0159\u0168\u016c"+
		"\u0182\u0195\u01a0\u01a9\u01b4\u01bd\u01eb\u01f8\u020c\u022c\u0236\u024e"+
		"\u0256\u0259\u0261\u0263\u026b\u026d\u0277\u0279\u0285\u0287\u0295\u02a3"+
		"\u02a9";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}